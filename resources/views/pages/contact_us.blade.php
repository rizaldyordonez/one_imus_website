<section class="parallax-container section scrollspy" id="contact_us">
	<div class="container">
		<div class="row">
			<form class="col s12" id="contact-form">
				<div class="row">
					<div class="col s12 m6">
						<div style="height:1rem;clear:both"></div>
						{{-- <iframe src="https://www.google.com/maps/embed?pb=!4v1554170295179!6m8!1m7!1s5WACyVQxD1Ul0Zk6uyXaxw!2m2!1d14.39533891794669!2d120.9391438096748!3f187.67080808700211!4f-5.242067161115202!5f1.1924812503605782" width="100%" height="350px" frameborder="0" style="border:0" allowfullscreen></iframe> --}}
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d241.5345008198961!2d120.93905110159152!3d14.395312004128481!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397d3099d101d1b%3A0x3c687d004f7ccfd9!2sAnabu+1+-+B!5e0!3m2!1sen!2sph!4v1554294917736!5m2!1sen!2sph" width="100%" height="350px" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
					<div class="col s12 m6">
						<div class="row">
							<div class="col s12 wow slideInDown" data-wow-duration="2.5" data-wow-delay="1">
								<h4 class="section section_custo"><i class="material-icons prefix">local_phone</i>Contact Us</h4>
								<div class="divider"></div>
							</div>
							<div class="clear_both"></div>
							<div class="input-field col s12 ">
								<i class="material-icons prefix wow slideInLeft" data-wow-duration="3.5" data-wow-delay="1">person</i>
								<input placeholder="{{'@email.com'}}" id="email" name="email" type="email" class="validate wow">
								<label for="email">E-mail address</label>
								<span class="helper-text wow fadeInDown" data-wow-duration="3.5" data-wow-delay="3" data-error="Please use proper e-mail address" data-success="">E-mail address only</span>
							</div>
							<div class="input-field col s12 ">
								<i class="material-icons prefix wow slideInLeft" data-wow-duration="3.5" data-wow-delay="3">edit</i>
								<textarea placeholder="{{'Your message'}}" id="textarea1" name="message" class="materialize-textarea contact-message" data-wow-duration="2.5" data-wow-delay="3"></textarea>
								<label for="textarea1"></label>
							</div>
							<div class="right">
								<span id="contactus_message"></span>
								<a class="waves-effect waves-light btn wow slideInLeft" id="contact_us_btn" data-wow-duration="3.5" data-wow-delay="3" onclick="sendContact()"><i class="material-icons right">send</i>Submit</a>
							</div>
						</div>
						
					</div>
				</div>
			</form>
		</div>
	</div>

	<div class="parallax">
		<img src="{{ asset('images/HD/blank_right.jpeg') }}">
	</div>
</section>