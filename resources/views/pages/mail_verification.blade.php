<p>
	<p> Hi, {{ $name }} </p>

	<p>Welcome to ONEIMUS Job Contracting Services!</p>

	<p>You are just one step away from activating your account! </p>

	<p>To confirm your email account please click <a href="{{ route('verifyMail', ['code' => $code]) }}" target="_blank"> here </a></p>

	<ul style="padding: 0;">
		<p>In this website you can:</p>
		<li>Upload your own PDF File format Curriculum Vitae/ Resume.</li>
		<li>Access the website 24/7.</li>
		<li>Choose your desired position.</li>
		<li>See available slots for your desired position.</li>
	</ul>    

	<p>NOTE: This email is auto-generated email. You must not reply to this email.</p>

	<p>Best Regards,</p>

	<p>Oneimus team</p>
</p>