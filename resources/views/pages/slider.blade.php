<div class="carousel dim_bg carousel-slider center scrollspy" id="home">
	@foreach ($slider as $index=>$value)
	<div class="carousel-item teal white-text wow fadeIn" href="#one!">
		@php
			$url = 'images/sliders/'.$value->slider_img;
		@endphp
		<img class="responsive-img" src="{{ asset($url) }}">
		<div class="carousel-fixed-item center">
			<h2 class="wow fadeInDown" data-wow-duration="1s" data-wow-delay="1s">{{ $value->slider_desc }}</h2>
		</div>
	</div>
	@endforeach
</div>