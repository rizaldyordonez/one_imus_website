@extends('layouts.main')
@section('content')

<section class="container">
	<div class="col s12 m12">
		<div class="row">
			<div class="clear_both"></div>
			<div class="verify-center">
				<h4 class="message-h4">{{ $message }}</h4>
				@if ($success)
				<div class="clear_both">
					<i class="fa fa-check-circle icon-size font-green"></i>
				</div>
				@else
				<div class="clear_both">
					<i class="fa fa-exclamation-circle icon-size font-red"></i>
				</div>
				<br>
				<br>
				<br>
				<br>
				<span>
					If you didn't received verification email or you have some sort of issues? 
					<br>
					Please contact our support or re-send verification <a href="{{ route('reverifyMail') }}">here</a>.
				</span>
				@endif
			</div>
		</div>
	</div>
</section>
@endsection

@section('customCSS')
<style type="text/css">
	.font-green{
		color: #3bd416;
	}
	.font-red{
		color: #f70303;
	}
	.table-body{
		overflow-x: scroll;
		height: 312px;
	}
	.form-modal{
		min-width: 40%;
	}
	.message{
		float: left;
	}
	.message-h4{
		color: rgba(0,0,0,0.87);
		font-size: 2rem;
		line-height: 110%;
		margin: 1.52rem 0 1.912rem 0;
	}
	.verify-center{
		margin: 0 auto;
		width: 60%;
		text-align: center;
	}
	.icon-size{
		font-size: 70px;
	}
</style>
@endsection

@section('customJS')
<script type="text/javascript">

</script>
@endsection