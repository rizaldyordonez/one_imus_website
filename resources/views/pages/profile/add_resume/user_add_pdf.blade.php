@extends('layouts.main')
@section('customCSS')
<style type="text/css">
	/*.tabs {
		height: 60px!important;
	}*/
</style>
@endsection
@section('content')

<section class="container">
	<div class="col s12 m12">
		<div class="row">
			<div class="col s12 m12">
				<div class="left"><h2> @if(isset($pdf)) RE - @endif UPLOAD PDF/RESUME </h2></div>
				{{-- <div class="clear_both_1"></div>
				<div>{{ strtoupper(Auth::user()->name) }}</div>
				<div class="clear_both_1"></div>
				<div>{{ Auth::user()->email }}</div>
				<div class="clear_both_1"></div> --}}
				
			</div>
		</div>
		<div class="row">
			@if (count($errors) > 0)
			<div class="alert alert-danger" style="margin-top:15px;max-height:70px;overflow-y: scroll;">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
			<form role="form" method="POST" action="{{-- route('doInsertPDF') --}}" enctype="multipart/form-data">
				@csrf
				<input type="hidden" name="id" value="@if(isset($pdf)){{ $pdf->resume_id }}@endif">
				<div class="col s12">
					<div class="file-field input-field">
						<div class="btn">
							<span>Upload your PDF</span>
							<input class="main-image" type="file" id="img-5" name="resume_pdf">
						</div>
						<div class="file-path-wrapper">
							<input id="img-path-5" class="file-path validate" type="text" value="{{old('resume_pdf')}}">
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	
</section>

@endsection

@section('customCSS')
<style type="text/css">

</style>
@endsection

@section('customJS')
<script type="text/javascript">

</script>
@endsection