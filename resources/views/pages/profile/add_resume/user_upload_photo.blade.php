<div class="row">
	<div class="col m4">
		<label for="mainImage">
			<img src="http://catawbabrewing.com/wp-content/themes/catawba/images/placeholder.png"  alt="Placeholder" id="preview-img-5" class="placeholder img-thumbnail responsive-img image-preview" aria-describedby="imagePreviewHelp" /></label>
		<small id="imagePreviewHelp" class="form-text text-muted">Preview could appear stretched</small>
	</div>
	<div class="col m8">
		<div class="file-field input-field">
			<div class="btn">
				<span>Upload your photo</span>
				<input class="main-image" type="file" id="img-5" name="resume_photo">
			</div>
			<div class="file-path-wrapper">
				<input id="img-path-5" class="file-path validate" type="text" value="{{old('resume_photo')}}">
			</div>
		</div>
	</div>
	<div style="float:right">
		<a class="btn waves-effect waves-light clearUpload" id="clearUpload-5">Clear<i class="material-icons right">close</i></a>
	</div>
</div>
	
<div class="row">
	<div class="input-field col s12">
		<button type="submit" class="btn waves-effect waves-light" id="toSubmit">
			SUBMIT <i class="material-icons right">send</i>
		</button>
	</div>
</div>