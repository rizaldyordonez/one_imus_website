<div class="row">
	<div class="append-container-skills">
			@if(isset($cv))
				@php
					$skills = json_decode($cv->resume_skills, true);
					$skills_name = $skills['name'];
				@endphp
				@foreach($skills_name as $index=>$val)
				<div>
					<div class="input-field col s6">
						<input  id="skills_{{ $index }}" name="skills[name][]" type="text" class="validate no_special_charNum" value="{{ $val }}">
						@if ($errors->has('skills'))
						<span class="help-block">
							<strong>{{ $errors->first('skills') }}</strong>
						</span>
						@endif
						<label for="skills_{{ $index }}">Your skill</label>
					</div>
					<div class="input-field col s6"> 
						<input  id="yearsOfXP_0" name="skills[year][]" type="text" maxlength="2" class="validate digits_only" value="{{ $skills['year'][$index] }}">
						@if ($errors->has('skills'))
						<span class="help-block">
							<strong>{{ $errors->first('skills') }}</strong>
						</span>
						@endif
						<label for="yearsOfXP_{{ $index }}">Years of experience</label>
					</div>
				</div>
				@endforeach
			@elseif(old('skills'))
					@php
						$old_skills = old('skills');
						$old_skills_name = $old_skills['name'];
					@endphp
					@if(isset($old_skills))
						@foreach($old_skills_name as $index => $old_skills_name)
						<div>
							<div class="input-field col s6">
								<input  id="skills_{{ $index }}" name="skills[name][]" type="text" class="validate no_special_charNum" value="{{ $old_skills_name }}">
								@if ($errors->has('skills'))
								<span class="help-block">
									<strong>{{ $errors->first('skills') }}</strong>
								</span>
								@endif
								<label for="skills_{{ $index }}">Your skill</label>
							</div>
							<div class="input-field col s6">
								<input  id="yearsOfXP_{{ $index }}" name="skills[year][]" type="text" maxlength="2" class="validate digits_only" value="{{ $old_skills['year'][$index] }}">
								@if ($errors->has('yearsOfXP'))
								<span class="help-block">
									<strong>{{ $errors->first('yearsOfXP') }}</strong>
								</span>
								@endif
								<label for="yearsOfXP_{{ $index }}">Years of experience</label>
							</div>
						</div>
						@endforeach
					@endif
			@else
			<div>
				<div class="input-field col s6">
					<input  id="skills_0" name="skills[name][]" type="text" class="validate no_special_charNum">
					@if ($errors->has('skills'))
					<span class="help-block">
						<strong>{{ $errors->first('skills.name') }}</strong>
					</span>
					@endif
					<label for="skills_0">Your skill</label>
				</div>
				<div class="input-field col s6"> 
					<input  id="yearsOfXP_0" name="skills[year][]" type="text" maxlength="2" class="validate digits_only">
					@if ($errors->has('yearsOfXP'))
					<span class="help-block">
						<strong>{{ $errors->first('skills.year') }}</strong>
					</span>
					@endif
					<label for="yearsOfXP_0">Years of experience</label>
				</div>
			</div>
			@endif
	</div>	
</div>
	
<div class="row">
	<div class="input-field col s12">
		<a class="btn waves-effect waves-light append-input-skills">
			ADD SKILL <i class="material-icons right">add</i>
		</a>
		
		<a class="btn waves-effect waves-light" id="toContactRefInfo">
			NEXT <i class="material-icons right">arrow_forward</i>
		</a>
	</div>
</div>