<div class="row">
	<div class="input-field col s12 m4">
		@if(isset($cv))
			<input  id="email" name="email" type="text" class="validate" maxlength="50" value="{{ $cv->resume_email }}">
		@else
			<input  id="email" name="email" type="text" class="validate" maxlength="50" value="{{ old('email') }}">
		@endif

			@if ($errors->has('email'))
			<span class="help-block">
				<strong>{{ $errors->first('email') }}</strong>
			</span>
			@endif
		<label for="email">Email address</label>
	</div>

	<div class="input-field col s12 m4">
		@if(isset($cv))
			<input  id="homeaddress" name="homeaddress" type="text" maxlength="40" class="validate sentence" value="{{ $cv->resume_homeaddress }}">
		@else
			<input  id="homeaddress" name="homeaddress" type="text" maxlength="40" class="validate sentence" value="{{ old('homeaddress') }}">
		@endif

			@if ($errors->has('homeaddress'))
			<span class="help-block">
				<strong>{{ $errors->first('homeaddress') }}</strong>
			</span>
			@endif
		<label for="homeaddress">Home Address</label>
	</div>
	
	<div class="input-field col s12 m4">
		@if(isset($cv->resume_city))
			<input name="city" id="city" type="text" class="validate sentence no_special_charNum" maxlength="20" value="{{ $cv->resume_city }}">
			<label for="city">City</label>
		@else
			<input name="city" id="city" type="text" class="validate sentence no_special_charNum" maxlength="20" value="{{ old('city') }}">
			<label for="city">City</label>
		@endif
	</div>
</div>
<div class="row">

	<div class="input-field col s12 m6">

		@if(isset($cv))
			<input  id="mobilenumber" name="mobilenumber" maxlength="11" type="text" class="validate digits_only" value="{{ $cv->resume_mobilenumber }}">
		@else
			<input  id="mobilenumber" name="mobilenumber" maxlength="11" type="text" class="validate digits_only" value="{{ old('mobilenumber') }}">
		@endif

			@if ($errors->has('mobilenumber'))
			<span class="help-block">
				<strong>{{ $errors->first('mobilenumber') }}</strong>
			</span>
			@endif
		<label for="mobilenumber">Mobile number</label>
	</div>

	<div class="input-field col s12 m6">
		@php
			  $regions[] =  "ARMM (Autonomous Region in Muslim Mindanao)";
			  $regions[] =  "CAR (Cordillera Administrative Region)";
			  $regions[] =  "NCR (National Capital Region)";
			  $regions[] =  "Region 1 (Ilocos Region)";
			  $regions[] =  "Region 2 (Cagayan Valley)";
			  $regions[] =  "Region 3 (Central Luzon)";
			  $regions[] =  "Region 4A (CALABARZON)";
			  $regions[] =  "Region 4B (MIMAROPA)";
			  $regions[] =  "Region 5 (Bicol Region)";
			  $regions[] =  "Region 6 (Western Visayas)";
			  $regions[] =  "Region 7 (Central Visayas)";
			  $regions[] =  "Region 9 (Zamboanga Peninsula)";
			  $regions[] =  "Region 10 (Northern Mindanao)";
			  $regions[] =  "Region 11 (Davao Region)";
			  $regions[] =  "Region 12 (SOCCSKSARGEN)";
			  $regions[] =  "Region 13 (Caraga Region)";	
		@endphp
		<select name="region" id="region">
			@if(isset($cv))
			<option disabled selected @if($cv->resume_region == "") selected @endif>Region</option>
			@foreach ($regions as $value)
				<option @if($cv->resume_region == $value ) selected @endif>{{ $value }}</option>
			@endforeach
			@else
				<option disabled selected>Region</option>
				@foreach ($regions as $value)
					<option @if(old('region') == $value) selected @endif> {{ $value }} </option>
				@endforeach
			@endif
		</select>
	</div>

	<div class="input-field col s6">
		@if(isset($cv))
			<input  id="resume_father" name="resume_father" type="text" maxlength="30" class="validate sentence no_special_charNum" value="{{ $cv->resume_father }}">
		@else
			<input  id="resume_father" name="resume_father" type="text" maxlength="30" class="validate sentence no_special_charNum" value="{{ old('resume_father') }}">
		@endif

			@if ($errors->has('resume_father'))
			<span class="help-block">
				<strong>{{ $errors->first('resume_father') }}</strong>
			</span>
			@endif
		<label for="resume_father">Father's name (Optional)</label>
	</div>

	<div class="input-field col s6">
		@if(isset($cv))
			<input  id="resume_father_contact" name="resume_father_contact" maxlength='11' type="text" class="validate digits_only" value="{{ $cv->resume_father_contact }}">
		@else
			<input  id="resume_father_contact" name="resume_father_contact" maxlength='11' type="text" class="validate digits_only" value="{{ old('resume_father_contact') }}">
		@endif

			@if ($errors->has('resume_father_contact'))
			<span class="help-block">
				<strong>{{ $errors->first('resume_father_contact') }}</strong>
			</span>
			@endif
		<label for="resume_father_contact">Father's contact (Optional)</label>
	</div>

	<div class="input-field col s6">
		@if(isset($cv))
			<input  id="resume_mother" name="resume_mother" type="text" maxlength="30" class="validate sentence no_special_charNum" value="{{ $cv->resume_mother }}">
		@else
			<input  id="resume_mother" name="resume_mother" type="text" maxlength="30" class="validate sentence no_special_charNum" value="{{ old('resume_mother') }}">
		@endif

			@if ($errors->has('resume_mother'))
			<span class="help-block">
				<strong>{{ $errors->first('resume_mother') }}</strong>
			</span>
			@endif
		<label for="resume_mother">Mother's name (Optional)</label>
	</div>

	<div class="input-field col s6">
		@if(isset($cv))
			<input  id="resume_mother_contact" name="resume_mother_contact" maxlength='11' type="text" class="validate digits_only" value="{{ $cv->resume_mother_contact }}">
		@else
			<input  id="resume_mother_contact" name="resume_mother_contact" maxlength='11' type="text" class="validate digits_only" value="{{ old('resume_mother_contact') }}">
		@endif

			@if ($errors->has('resume_mother_contact'))
			<span class="help-block">
				<strong>{{ $errors->first('resume_mother_contact') }}</strong>
			</span>
			@endif
		<label for="resume_mother_contact">Mother's contact (Optional)</label>
	</div>

	<div class="input-field col s12">
		<a class="btn waves-effect waves-light" id="toWorkInfo">
			NEXT <i class="material-icons right">arrow_forward</i>
		</a>
	</div>
</div>