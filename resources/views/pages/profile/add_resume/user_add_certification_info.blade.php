<div class="row">
	<div class="append-cert-container">
		<upload>
			<div class="row">
				<div class="col m4">
					<label for="mainImage"><img src="../images/pages/placeholder.png" alt="Placeholder" id="preview-img-0" 
						class="placeholder img-thumbnail responsive-img image-preview" aria-describedby="imagePreviewHelp" /></label>
					<small id="imagePreviewHelp" class="form-text text-muted">Preview could appear stretched</small>
				</div>
				<div class="col m8">
					<div class="file-field input-field">
						<div class="btn">
							<span>Upload your photo</span>
							<input class="main-image" type="file" id="img-0" name="cert[0]">
						</div>
						<div class="file-path-wrapper">
							<input id="img-path-0" class="file-path validate" type="text" value="{{old('cert[0]')}}">
						</div>
					</div>
				</div>
				<div style="float:right">
					<a class="btn waves-effect waves-light clearUpload" id="clearUpload-0">Clear<i class="material-icons right">close</i></a>
				</div>
			</div>
		</upload>
		
	</div>
</div>
	
<div class="row">
	<div class="input-field col s12">
		<a class="btn waves-effect waves-light append-input-certificate">
			ADD CERTIFICATE <i class="material-icons right">add</i>
		</a>
		<a class="btn waves-effect waves-light" id="toAchievementInfo">
			NEXT / SKIP<i class="material-icons right">arrow_forward</i>
		</a>
	</div>
</div>