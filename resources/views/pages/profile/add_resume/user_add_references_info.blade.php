<div class="row">
	<div class="append-container-references">
		@if(!is_null($cv))
			@php
			$references = json_decode($cv->resume_references, true);
			$references_name = $references['ref_name'];
			@endphp
		 
			@foreach($references_name as $index=>$val)
				<div>
				<div class="input-field col s12 m2">
					<input  id="ref_name_{{ $index }}" name="ref_data[ref_name][]" type="text" class="validate no_special_charNum" value="{{ $val }}">
					@if ($errors->has('ref_name'))
					<span class="help-block">
						<strong>{{ $errors->first('ref_name') }}</strong>
					</span>
					@endif
					<label for="ref_name_{{ $index }}">Name</label>
				</div>
				<div class="input-field col s12 m2"> 
					<input  id="ref_relation_{{ $index }}" name="ref_data[ref_relation][]" type="text" class="validate no_special_charNum" value="{{ $references['ref_relation'][$index] }}">
					@if ($errors->has('ref_relation'))
					<span class="help-block">
						<strong>{{ $errors->first('ref_relation') }}</strong>
					</span>
					@endif
					<label for="ref_relation_{{ $index }}">Relationship</label>
				</div>
				<div class="input-field col s12 m2"> 
					<input  id="ref_company_{{ $index }}" name="ref_data[ref_company][]" type="text" class="validate no_special_char" value="{{ $references['ref_company'][$index] }}">
					@if ($errors->has('ref_company'))
					<span class="help-block">
						<strong>{{ $errors->first('ref_company') }}</strong>
					</span>
					@endif
					<label for="ref_company_{{ $index }}">Company</label>
				</div>
				<div class="input-field col s12 m2"> 
					<input  id="ref_position_{{ $index }}" name="ref_data[ref_position][]" type="text" class="validate no_special_charNum" value="{{ $references['ref_position'][$index] }}">
					@if ($errors->has('ref_position'))
					<span class="help-block">
						<strong>{{ $errors->first('ref_position') }}</strong>
					</span>
					@endif
					<label for="ref_position_{{ $index }}">Position</label>
				</div>
				<div class="input-field col s12 m2"> 
					<input  id="ref_phone_{{ $index }}" name="ref_data[ref_phone][]" maxlength='11' type="text" class="validate digits_only" value="{{ $references['ref_phone'][$index] }}">
					@if ($errors->has('ref_phone'))
					<span class="help-block">
						<strong>{{ $errors->first('ref_phone') }}</strong>
					</span>
					@endif
					<label for="ref_phone_{{ $index }}">Contact</label>
				</div>
				<div class="input-field col s12 m2"> 
					<input  id="ref_email_{{ $index }}" name="ref_data[ref_email][]" type="text" class="validate" value="{{ $references['ref_email'][$index] }}">
					@if ($errors->has('ref_email'))
					<span class="help-block">
						<strong>{{ $errors->first('ref_email') }}</strong>
					</span>
					@endif
					<label for="ref_email_{{ $index }}">Email</label>
				</div>
				</div>
			@endforeach
		@elseif(old('ref_data'))
			@php
				$old_references = old('ref_data');
				$old_references_name = $old_references['ref_name'];
			@endphp

			@foreach($old_references_name as $index => $ref_data)
				<div class="input-field col s12 m2">
					<input  id="ref_name_{{ $index }}" name="ref_data[ref_name][]" type="text" class="validate no_special_charNum" value="{{ $ref_data }}">
					@if ($errors->has('ref_name'))
					<span class="help-block">
						<strong>{{ $errors->first('ref_name') }}</strong>
					</span>
					@endif
					<label for="ref_name_{{ $index }}">Name</label>
				</div>
				<div class="input-field col s12 m2"> 
					<input  id="ref_relation_{{ $index }}" name="ref_data[ref_relation][]" type="text" class="validate no_special_charNum" value="{{ $old_references['ref_relation'][$index] }}">
					@if ($errors->has('ref_relation'))
					<span class="help-block">
						<strong>{{ $errors->first('ref_relation') }}</strong>
					</span>
					@endif
					<label for="ref_relation_{{ $index }}">Relationship</label>
				</div>
				<div class="input-field col s12 m2"> 
					<input  id="ref_company_{{ $index }}" name="ref_data[ref_company][]" type="text" class="validate no_special_char" value="{{ $old_references['ref_company'][$index] }}">
					@if ($errors->has('ref_company'))
					<span class="help-block">
						<strong>{{ $errors->first('ref_company') }}</strong>
					</span>
					@endif
					<label for="ref_company">Company</label>
				</div>
				<div class="input-field col s12 m2"> 
					<input  id="ref_position_{{ $index }}" name="ref_data[ref_position][]" type="text" class="validate no_special_charNum" value="{{ $old_references['ref_position'][$index] }}">
					@if ($errors->has('ref_position'))
					<span class="help-block">
						<strong>{{ $errors->first('ref_position') }}</strong>
					</span>
					@endif
					<label for="ref_position_{{ $index }}">Position</label>
				</div>
				<div class="input-field col s12 m2"> 
					<input  id="ref_phone_{{ $index }}" name="ref_data[ref_phone][]" maxlength='11' type="text" class="validate digits_only" value="{{ $old_references['ref_phone'][$index] }}">
					@if ($errors->has('ref_phone'))
					<span class="help-block">
						<strong>{{ $errors->first('ref_phone') }}</strong>
					</span>
					@endif
					<label for="ref_phone_{{ $index }}">Contact</label>
				</div>
				<div class="input-field col s12 m2"> 
					<input  id="ref_email_{{ $index }}" name="ref_data[ref_email][]" type="text" class="validate" value="{{ $old_references['ref_email'][$index] }}">
					@if ($errors->has('ref_email'))
					<span class="help-block">
						<strong>{{ $errors->first('ref_email') }}</strong>
					</span>
					@endif
					<label for="ref_email_{{ $index }}">Email</label>
				</div>
			@endforeach
	    @else
			<div class="input-field col s12 m2">
				<input  id="ref_name_0" name="ref_data[ref_name][]" type="text" class="validate no_special_charNum">
				@if ($errors->has('ref_name'))
				<span class="help-block">
					<strong>{{ $errors->first('ref_name') }}</strong>
				</span>
				@endif
				<label for="ref_name_0">Name</label>
			</div>
			<div class="input-field col s12 m2"> 
				<input  id="ref_relation_0" name="ref_data[ref_relation][]" type="text" class="validate no_special_charNum">
				@if ($errors->has('ref_relation'))
				<span class="help-block">
					<strong>{{ $errors->first('ref_relation') }}</strong>
				</span>
				@endif
				<label for="ref_relation_0">Relationship</label>
			</div>
			<div class="input-field col s12 m2"> 
				<input  id="ref_company_0" name="ref_data[ref_company][]" type="text" class="validate no_special_char">
				@if ($errors->has('ref_company'))
				<span class="help-block">
					<strong>{{ $errors->first('ref_company') }}</strong>
				</span>
				@endif
				<label for="ref_company_0">Company</label>
			</div>
			<div class="input-field col s12 m2"> 
				<input  id="ref_position_0" name="ref_data[ref_position][]" type="text" class="validate no_special_charNum">
				@if ($errors->has('ref_position'))
				<span class="help-block">
					<strong>{{ $errors->first('ref_position') }}</strong>
				</span>
				@endif
				<label for="ref_position_0">Position</label>
			</div>
			<div class="input-field col s12 m2"> 
				<input  id="ref_phone_0" name="ref_data[ref_phone][]" maxlength='11' type="text" class="validate digits_only" >
				@if ($errors->has('ref_phone'))
				<span class="help-block">
					<strong>{{ $errors->first('ref_phone') }}</strong>
				</span>
				@endif
				<label for="ref_phone_0">Contact</label>
			</div>
			<div class="input-field col s12 m2"> 
				<input  id="ref_email_0" name="ref_data[ref_email][]" type="text" class="validate">
				@if ($errors->has('ref_email'))
				<span class="help-block">
					<strong>{{ $errors->first('ref_email') }}</strong>
				</span>
				@endif
				<label for="ref_email_0">Email</label>
			</div>
		@endif
	</div>
</div>

<div class="row">
	<div class="input-field col s12">
		<a class="btn waves-effect waves-light append-input-references">
			ADD REFERENCE <i class="material-icons right">add</i>
		</a>
		<a class="btn waves-effect waves-light" id="toUpload">
			 NEXT <i class="material-icons right">arrow_forward</i>
		</a>
	</div>
</div>