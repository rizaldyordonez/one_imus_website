@extends('layouts.main')
@section('customCSS')
<style type="text/css">
	/*.tabs {
		height: 60px!important;
	}*/
</style>
@endsection
@section('content')

<section class="container">
	<div class="col s12 m12">
		<div class="row">
			<div class="col s6 m6">
				<div class="left"><h2> @if(isset($cv)) EDIT @else ADD @endif RESUME </h2></div>
				{{-- <div class="clear_both_1"></div>
				<div>{{ strtoupper(Auth::user()->name) }}</div>
				<div class="clear_both_1"></div>
				<div>{{ Auth::user()->email }}</div>
				<div class="clear_both_1"></div> --}}
				
			</div>
		</div>
		<div class="row">
			@if (count($errors) > 0)
			<div class="alert alert-danger" style="margin-top:15px;max-height:70px;overflow-y: scroll;">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
			<form role="form" method="POST" action="{{ route('doInsertResume') }}" enctype="multipart/form-data">
				@csrf
				<input type="hidden" name="id" value="@if(isset($cv)){{ $cv->resume_id }}@endif">
			<div class="col s12">
				<ul class="tabs">
					<li class="tab s1"><a class="active" href="#personalInfo">Personal Info</a></li>
					<li class="tab s1"><a href="#contactInfo">Contact Info</a></li>
					<li class="tab s1"><a href="#workInfo">Work Info</a></li>
					<li class="tab s1"><a href="#educationInfo">Education</a></li>
					<li class="tab s1"><a href="#certificationInfo">Certification</a></li>
					<li class="tab s1"><a href="#achievementInfo">Achievement</a></li>
					<li class="tab s1"><a href="#skillsInfo">Skills and Languages</a></li>
					<li class="tab s1"><a href="#referencesInfo">Contact References</a></li>
					<li class="tab s1"><a href="#uploadPhoto">Upload your Photo</a></li>
				</ul>
			</div>

			<div class="container">
				<div id="personalInfo" class="col s12">
					@include('pages.profile.add_resume.user_add_profile')
				</div>
				<div id="contactInfo" class="col s12 d-none">
					@include('pages.profile.add_resume.user_add_contact_info')
				</div>
				<div id="workInfo" class="col s12 d-none">
					@include('pages.profile.add_resume.user_add_work_info')
				</div>
				<div id="educationInfo" class="col s12 d-none">
					@include('pages.profile.add_resume.user_add_education_info')
				</div>
				<div id="certificationInfo" class="col s12 d-none">
					@include('pages.profile.add_resume.user_add_certification_info')
				</div>
				<div id="achievementInfo" class="col s12 d-none">
					@include('pages.profile.add_resume.user_add_achievement_info')
				</div>
				<div id="skillsInfo" class="col s12 d-none">
					@include('pages.profile.add_resume.user_add_skills_info')
				</div>
				<div id="referencesInfo" class="col s12 d-none">
					@include('pages.profile.add_resume.user_add_references_info')
				</div>
				<div id="uploadPhoto" class="col s12 d-none">
					@include('pages.profile.add_resume.user_upload_photo')
				</div>
			</div>

			</form>
		</div>
	</div>
	
</section>

@php
	
	if(isset($cv->resume_skills)) 
	{
		$resume_skills = json_decode($cv->resume_skills);
		$resume_skills = count($resume_skills->name);
	}else{
		$resume_skills = null;
	}

	if(isset($cv->resume_references))
	{
		$resume_references = json_decode($cv->resume_references);
		$resume_references = count($resume_references->ref_name);
	}else{
		$resume_references = null;
	}

	if(isset($cv->resume_edu_attain))
	{
		$educ = $cv->resume_edu_attain;
	}else{
		$educ = 0;
	}

@endphp

@endsection

@section('customCSS')
<style type="text/css">
	.table-body{
		overflow-x: scroll;
		height: 312px;
	}
	.form-modal{
		min-width: 40%;
	}
	.message{
		float: left;
	}
	.switch-margin-left {
		margin-left: 15px;
	}
	.switch-width{
		width: 185px;
	}
	.textarea-objective{
		background-color: aliceblue;
		border-radius: 10px;
	}
	.textarea-objective textarea{
		border-bottom: none;
		height:75px!important;
	}
	tr.orange>th {
		border-right: 1px solid white;
	}
</style>
@endsection

@section('customJS')
<script type="text/javascript">

	$(document).ready(function(){

	$('.datepicker').datepicker({ yearRange: 50 });
	$('.datepicker_year').datepicker({format: 'yyyy',yearRange:50});


	var educ = "{{ $educ }}";
	checkEduc(educ);
	

	var max_fields  = 4;
	var wrapper_prevJobs  = $(".append-container-prevjobs");
	var prevJobs = @if(isset($cv) && !is_null($cv->resume_prevjobs)) {{ count(json_decode($cv->resume_prevjobs)) }} @else 1 @endif;
	
	var wrapper_achievements  = $(".append-container-achievements");
	var achievements = @if(isset($cv) && !is_null($cv->resume_achievement)) {{ count(json_decode($cv->resume_achievement)) }} @else 1 @endif;

	var wrapper_certificates = $(".append-cert-container");
	var certificate = wrapper_certificates.children('upload').length;

	var wrapper_skills = $(".append-container-skills");
	var skills = @if(!is_null($resume_skills)) {{ $resume_skills }} @else 1 @endif;
	
	var wrapper_references = $(".append-container-references");
	var references = @if(!is_null($resume_references)) {{ $resume_references }} @else 1 @endif;

	//hindi pa tapos upload certificate
	$('.append-input-certificate').click(function(e){

		e.preventDefault();
		console.log(wrapper_certificates.children('div').length);
		if(wrapper_certificates.children('upload').length < max_fields){
			
			var html = '';

			// html += '<upload><div class="file-field input-field"><div class="btn"><span>Upload your photo</span><input type="file" id="img-'+ c +'" name="cert['+ c +']"></div>';

			// html += '<div class="file-path-wrapper"><input id="img-path-'+ c +'" class="file-path validate" type="text" value=""><a class="btn waves-effect waves-light delete" id="deleteHide">Delete<i class="material-icons right">close</i></a></div></div></upload>';
			html += '<upload><div class="row"><div class="col m4">';
			html += '<label for="mainImage"><img src="http://catawbabrewing.com/wp-content/themes/catawba/images/placeholder.png" alt="Placeholder" id="preview-img-'+ certificate +'" class="placeholder img-thumbnail responsive-img image-preview" aria-describedby="imagePreviewHelp" /></label>';
			html += '<small id="imagePreviewHelp" class="form-text text-muted">Preview could appear stretched</small></div>';
			html += '<div class="col m8">';
			html += '<div class="file-field input-field">';
			html += '<div class="btn">';
			html += '<span>Upload your photo</span>';
			html += '<input class="main-image" type="file" id="img-'+ certificate +'" name="cert['+ certificate +']"></div>';
			html += '<div class="file-path-wrapper">';
			html += '<input id="img-path-'+ certificate +'" class="file-path validate" type="text"> </div>';
			html += '</div></upload>';
			html += '<div style="float:right"><a class="btn waves-effect waves-light delete" id="deleteHide">Delete<i class="material-icons right">close</i></a></div>';
			html += '<div style="float:right;margin-right:10px;"><a class="btn waves-effect waves-light clearUpload" id="clearUpload-'+ certificate +'">Clear<i class="material-icons right">close</i></a></div></div>';
			$(wrapper_certificates).append(html);
			certificate++;
		}else{
			alert('You reached the limit.')
		}

		$(wrapper_certificates).on("click","#deleteHide", function(e){
			e.preventDefault(); $(this).parent().parent().parent().parent('upload').remove();
		})
	});
		
	//seems ok
	$('.append-input-prevjobs').click(function(e){
		
		e.preventDefault();
		console.log(wrapper_prevJobs.children('div').length);
		if(wrapper_prevJobs.children('div').length < max_fields){
			var html = '';

			html += '<div>' +
			'<div class="input-field col s3">' +
			'<input  id="companyName_'+ prevJobs +'" maxlength="20"  name="prevDetails['+ prevJobs +'][]" type="text" class="validate no_special_charNum">'+
			'<label for="companyName_'+ prevJobs +'">Company</label>' +
			'</div>';

			html += '<div class="input-field col s3 appended">' +
			'<input  id="job_'+ prevJobs +'" maxlength="20" name="prevDetails['+ prevJobs +'][]" type="text" class="validate no_special_charNum">' +
			'<label for="job_'+ prevJobs +'">Job title</label>' +
			'</div>';

			html += '<div class="input-field col s3 appended">' +
			'<input  id="prev_yearFrom_'+ prevJobs +'" name="prevDetails['+ prevJobs +'][]" type="text" maxlength="4" class="validate prevYearfrom digits_only" placeholder="(Year)">' +
			'</div>';

			html += '<div class="input-field col s3 appended">' +
			'<input  id="prev_yearTo_'+ prevJobs +'" name="prevDetails['+ prevJobs +'][]" type="text" maxlength="4" class="validate prevYearTo digits_only" placeholder="(Year)">' +
			'</div>';

			html += '<div style="margin:0 auto;width:116px;"><a class="btn waves-effect waves-light delete" id="deleteHide">Delete<i class="material-icons right">close</i></a></div><br/></div>';
			$(wrapper_prevJobs).append(html);

			prevJobs++;
		}
		else
		{
			alert('You reached the limit.')
		}

		$(wrapper_prevJobs).on("click","#deleteHide", function(e){
			e.preventDefault(); $(this).parent().parent('div').remove();
		})
	}); 

	//seems ok
	$('.append-input-achievements').click(function(e){
		
		e.preventDefault();
		if(wrapper_achievements.children('div').length < max_fields){
			var html = '';

			html += '<div id="'+ achievements +'">' +
			'<input  id="achievement_'+ achievements +'" name="achievement['+achievements+'][]" type="text" class="validate no_special_charNum" >' +
			'<label for="achievement_'+ achievements +'">Your achievement (optional)</label>';

			html += '<div style="margin:0 auto;width:116px;"><a class="btn waves-effect waves-light delete" id="deleteHide">Delete<i class="material-icons right">close</i></a></div><br/></div>';
			$(wrapper_achievements).append(html);
			achievements++;
		}
		else
		{
			alert('You reached the limit.')
		}

		$(wrapper_achievements).on("click","#deleteHide", function(e){
			e.preventDefault(); $(this).parent().parent('div').remove();
		})
	}); 

	//seems ok
	$('.append-input-skills').click(function(e){

		e.preventDefault();
		console.log(wrapper_skills.children('div').length);
		if(wrapper_skills.children('div').length < max_fields){
			
			var html = '';
			
			html += '<div>'+
			'<div class="input-field col s6">' +
			'<input  id="skills_'+ skills +'" name="skills[name][]" type="text" class="validate no_special_charNum">' +
			'<label for="skills_'+ skills +'">Your skill</label>' +
			'</div>';

			html += '<div class="input-field col s6">' +
			'<input  id="yearsOfXP_'+ skills +'" name="skills[year][]" type="text" maxlength="2" class="validate digits_only">' +
			'<label for="yearsOfXP_'+ skills +'">Years of experience</label>' +
			'</div>';

			html += '<div style="margin:0 auto;width:116px;"><a class="btn waves-effect waves-light delete" id="deleteHide">Delete<i class="material-icons right">close</i></a></div><br/></div>';
			$(wrapper_skills).append(html);
			skills++;
		}else{
			alert('You reached the limit.')
		}

		$(wrapper_skills).on("click","#deleteHide", function(e){
			e.preventDefault(); $(this).parent().parent('div').remove();
		})
	});

	//seems ok
	$('.append-input-references').click(function(e){

		e.preventDefault();
		console.log(wrapper_references.children('div').length);
		if(wrapper_references.children('div').length < max_fields){
			var html = '';

			html += '<div>'+
			'<div class="input-field col s12 m2">' +
			'<input  id="ref_name_'+references+'" name="ref_data[ref_name][]" type="text" class="validate ref_name no_special_charNum">' +
			'<label for="ref_name_'+references+'">Name</label>' +
			'</div>';

			html += '<div class="input-field col s12 m2">' +
			'<input  id="ref_relation_'+references+'" name="ref_data[ref_relation][]" type="text" class="validate ref_relation no_special_charNum">' +
			'<label for="ref_relation_'+references+'">Relationship</label>' +
			'</div>';

			html += '<div class="input-field col s12 m2">' +
			'<input  id="ref_company_'+references+'" name="ref_data[ref_company][]" type="text" class="validate ref_company no_special_char">' +
			'<label for="ref_company_'+references+'">Company</label>' +
			'</div>';

			html += '<div class="input-field col s12 m2">' +
			'<input  id="ref_position_'+references+'" name="ref_data[ref_position][]" type="text" class="validate ref_position no_special_charNum">' +
			'<label for="ref_position_'+references+'">Position</label>' +
			'</div>';

			html += '<div class="input-field col s12 m2">' +
			'<input id="ref_phone_'+references+'" name="ref_data[ref_phone][]" type="text" maxlength="11" class="validate digits_only">' +
			'<label for="ref_phone_'+references+'">Contact</label>' +
			'</div>';

			html += '<div class="input-field col s12 m2">' +
			'<input  id="ref_email_'+references+'" name="ref_data[ref_email][]" type="text" class="validate ref_email">' +
			'<label for="ref_email_'+references+'">Email</label>' +
			'</div>';

			html += '<div style="margin:0 auto;width:116px;"><a class="btn waves-effect waves-light delete" id="deleteHide">Delete<i class="material-icons right">close</i></a></div><br/></div>';
			$(wrapper_references).append(html);
			references++;
		}
		else
		{
			alert('You reached the limit.')
		}

		$(wrapper_references).on("click","#deleteHide", function(e){
			e.preventDefault(); $(this).parent().parent('div').remove();
		})
	});

	$('#educationalAttainment').change(function(e){
		var educ;
		var check;
		var array_educ = ['Elementary Under Graduate','Elementary Graduate','High School Under Graduate','High School Graduate'];
		var study = $('#fieldOfStudy');
		educ = $('#educationalAttainment').val();

		check = jQuery.inArray(educ, array_educ);

		if (check >= 0) { 
			study.parent().find('input.select-dropdown').prop('disabled', true).val('');
		}else{
			study.parent().find('input.select-dropdown').prop('disabled', false).val('Choose Field of study');
		}
	});
	
	//NO SPECIAL CHARACTERS AND NUMBERS
	$(document).on('keyup','.no_special_charNum',function(str){
		var th = $(this);
		th.val( th.val().replace(/[^a-zÑñA-Z\s]/g, function(str) { 
			alert('No special characters and numbers'); return '';
		}));
	});

	$(document).on('keyup','.no_special_char',function(str){
		var th = $(this);
		th.val( th.val().replace(/[^a-zÑñA-Z0-9\s]/g, function(str) { 
			alert('No special characters'); return '';
		}));
	});

	//DIGITS ONLY
	$(document).on('keypress','.digits_only',function(e){
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57))
		{
			alert("Digits Only");
			return false;
		}
	});

	$(document).on('change','.main-image', function(){
		
		var id = $(this).attr('id');
		uploadPreview(this,id);
	});

	$(document).on('click','.clearUpload', function(){
		var id = $(this).attr('id');
		var new_id = id.slice(12,14);
		console.log(new_id);
		resetUploadPreview(new_id);
	});
});

	function checkEduc(educ)
	{
		if(educ>0)
		{
			var educ;
			var check;
			var array_educ = ['Elementary Under Graduate','Elementary Graduate','High School Under Graduate','High School Graduate'];
			var study = $('#fieldOfStudy');
			educ = $('#educationalAttainment').val();

			check = jQuery.inArray(educ, array_educ);

			if (check >= 0) { 
				study.parent().find('input.select-dropdown').prop('disabled', true).val('');
			}else{
				study.parent().find('input.select-dropdown').prop('disabled', false).val('Choose Field of study');
			}
		}
	};

	function uploadPreview(input,id)
	{
		var new_id = "#preview-"+id;
		
		if (input.files && input.files[0])
		{
			var reader = new FileReader();

			reader.onload = function (e)
			{
				if(e.target.result.trim().length == 0)
				{
					$(new_id).attr('src', '../images/pages/placeholder.png');
				}
				else
				{
					$(new_id).attr('src', e.target.result);
				}
			}

			reader.readAsDataURL(input.files[0]);
		}
	}

	function resetUploadPreview(id)
	{
		var new_id = "#preview-img-"+id;
		var input_id = "#img-path-"+id;
		$(input_id).val(null);
		$(new_id).attr('src', '../images/pages/placeholder.png');
	}


</script>
@endsection