<div class="row ">
	<div class="input-field col s12 append-container-achievements">
		<div>
			@if (!empty($cv->resume_achievement) && isset($cv))
				@foreach(json_decode($cv->resume_achievement) as $index => $element)
				<input  id="achievement_{{ $index }}" name="achievement[{{ $index }}][]" type="text" class="validate no_special_charNum" value="{{ $element[0] }}">
				@if ($errors->has('achievement'))
				<span class="help-block">
					<strong>{{ $errors->first('achievement') }}</strong>
				</span>
				@endif
				<label for="achievement">Your achievement (optional)</label>
				@endforeach
			@elseif(old('achievement'))
				@foreach (old('achievement') as $index=>$element)
				<input  id="achievement_{{ $index }}" name="achievement[{{ $index }}][]" type="text" class="validate no_special_charNum" value="{{ $element[0] }}">
				@if ($errors->has('achievement'))
				<span class="help-block">
					<strong>{{ $errors->first('achievement') }}</strong>
				</span>
				@endif
				<label for="achievement">Your achievement (optional)</label>
				@endforeach
			@else
				<input  id="achievement_0" name="achievement[0][]" type="text" class="validate no_special_charNum">
				@if ($errors->has('achievement'))
				<span class="help-block">
					<strong>{{ $errors->first('achievement') }}</strong>
				</span>
				@endif
				<label for="achievement">Your achievement (optional)</label>
			@endif
		</div>
	</div>
</div>
	
<div class="row">
	<div class="input-field col s12">

		<a class="btn waves-effect waves-light append-input-achievements">
			ACHIEVEMENT <i class="material-icons right">add</i>
		</a>
		<a class="btn waves-effect waves-light" id="toSkillsInfo">
			NEXT/SKIP <i class="material-icons right">arrow_forward</i>
		</a>
	</div>
</div>