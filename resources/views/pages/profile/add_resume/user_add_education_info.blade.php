<div class="row">
	<div class="input-field col s12 m6">
		<select name="educationalAttainment" id="educationalAttainment">

			@if(isset($cv))
				<option value="" disabled selected @if($cv->resume_edu_attain == "") selected @endif>Choose Educational Attainment</option>
				<option @if($cv->resume_edu_attain == "Elementary Under Graduate") selected @endif>Elementary Under Graduate</option>
				<option @if($cv->resume_edu_attain == "Elementary Graduate") selected @endif>Elementary Graduate</option>
				<option @if($cv->resume_edu_attain == "High School Under Graduate") selected @endif>High School Under Graduate</option>
				<option @if($cv->resume_edu_attain == "High School Graduate") selected @endif>High School Graduate</option>
				<option @if($cv->resume_edu_attain == "TECH Voc Graduate") selected @endif>TECH Voc Graduate</option>

				<option @if($cv->resume_edu_attain == "College Under Graduate") selected @endif>College Under Graduate</option>
				<option @if($cv->resume_edu_attain == "High School Under Graduate") selected @endif>High School Under Graduate</option>
				<option @if($cv->resume_edu_attain == "Bachelor's Degree") selected @endif>Bachelor's Degree</option>
				<option @if($cv->resume_edu_attain == "Masteral") selected @endif>Masteral</option>
				<option @if($cv->resume_edu_attain == "Doctoral") selected @endif>Doctoral</option>
			@else
				<option value="" disabled selected @if(old('educationalAttainment') == "") selected @endif>Choose Educational Attainment</option>
				<option @if(old('educationalAttainment') == "Elementary Under Graduate") selected @endif>Elementary Under Graduate</option>
				<option @if(old('educationalAttainment') == "Elementary Graduate") selected @endif>Elementary Graduate</option>
				<option @if(old('educationalAttainment') == "High School Under Graduate") selected @endif>High School Under Graduate</option>
				<option @if(old('educationalAttainment') == "High School Graduate") selected @endif>High School Graduate</option>
				<option @if(old('educationalAttainment') == "TECH Voc Graduate") selected @endif>TECH Voc Graduate</option>

				<option @if(old('educationalAttainment') == "College Under Graduate") selected @endif>College Under Graduate</option>
				<option @if(old('educationalAttainment') == "High School Under Graduate") selected @endif>High School Under Graduate</option>
				<option @if(old('educationalAttainment') == "Bachelor's Degree") selected @endif>Bachelor's Degree</option>
				<option @if(old('educationalAttainment') == "Masteral") selected @endif>Masteral</option>
				<option @if(old('educationalAttainment') == "Doctoral") selected @endif>Doctoral</option>
			@endif
		</select>
	</div>

	<div class="input-field col s12 m6">

		@if(isset($cv))
			<input  id="schoolname" name="schoolname" type="text" class="validate no_special_charNum" value="{{ $cv->resume_school }}">
		@else
			<input  id="schoolname" name="schoolname" type="text" class="validate no_special_charNum" value="{{ old('schoolname') }}">
		@endif

		@if ($errors->has('schoolname'))
		<span class="help-block">
			<strong>{{ $errors->first('schoolname') }}</strong>
		</span>
		@endif
		<label for="schoolname">School</label>
	</div>
</div>
<div class="row">
	<div class="input-field col s12 m6">
		<select name="fieldOfStudy" id="fieldOfStudy">
			@if(isset($cv))
				<option value="" disabled selected @if($cv->resume_field_study == null) selected @endif>Choose Field of study</option>
				<option @if($cv->resume_field_study == "Engineering (Civil)") selected @endif>Engineering (Civil)</option>
				<option @if($cv->resume_field_study == "Engineering (Electrical)") selected @endif>Engineering (Electrical)</option>
				<option @if($cv->resume_field_study == "Engineering (Mechanical)") selected @endif>Engineering (Mechanical)</option>
				<option @if($cv->resume_field_study == "Architecture") selected @endif>Architecture</option>
				<option @if($cv->resume_field_study == "Arts And Media") selected @endif>Arts and Media</option>
				<option @if($cv->resume_field_study == "Hotel And Restaurant Management") selected @endif>Hotel and Restaurant Management</option>
				<option @if($cv->resume_field_study == "Tourism Management") selected @endif>Tourism Management</option>
				<option @if($cv->resume_field_study == "Information Technology") selected @endif>Information Technology</option>
				<option @if($cv->resume_field_study == "Computer Science") selected @endif>Computer Science</option>
				<option @if($cv->resume_field_study == "Business Management Major in Financial Management") selected @endif>Business Management Major in Financial Management</option>
				<option @if($cv->resume_field_study == "Business Management Major in Operations Management") selected @endif>Business Management Major in Operations Management</option>
				<option @if($cv->resume_field_study == "Business Management Major in Human Resources") selected @endif>Business Management Major in Human Resources</option>
				<option @if($cv->resume_field_study == "Psychology") selected @endif>Psychology</option>
			@else
				<option value="" disabled selected @if(old('fieldOfStudy') == "") selected @endif>Choose Field of study</option>
				<option @if(old('fieldOfStudy') == "Engineering (Civil)") selected @endif>Engineering (Civil)</option>
				<option @if(old('fieldOfStudy') == "Engineering (Electrical)") selected @endif>Engineering (Electrical)</option>
				<option @if(old('fieldOfStudy') == "Engineering (Mechanical)") selected @endif>Engineering (Mechanical)</option>
				<option @if(old('fieldOfStudy') == "Architecture") selected @endif>Architecture</option>
				<option @if(old('fieldOfStudy') == "Arts and Media") selected @endif>Arts and Media</option>
				<option @if(old('fieldOfStudy') == "Hotel and Restaurant Management") selected @endif>Hotel and Restaurant Management</option>
				<option @if(old('fieldOfStudy') == "Tourism Management") selected @endif>Tourism Management</option>
				<option @if(old('fieldOfStudy') == "Information Technology") selected @endif>Information Technology</option>
				<option @if(old('fieldOfStudy') == "Computer Science") selected @endif>Computer Science</option>
				<option @if(old('fieldOfStudy') == "Business Management Major in Financial Management") selected @endif>Business Management Major in Financial Management</option>
				<option @if(old('fieldOfStudy') == "Business Management Major in Operations Management") selected @endif>Business Management Major in Operations Management</option>
				<option @if(old('fieldOfStudy') == "Business Management Major in Human Resources") selected @endif>Business Management Major in Human Resources</option>
				<option @if(old('fieldOfStudy') == "Psychology") selected @endif>Psychology</option>
			@endif
		</select>
	</div>

	<div class="input-field col s12 m3">
		@if(isset($cv))
			<input  id="graduateDateFrom" name="graduateDateFrom" maxlength='4' type="text" class="validate digits_only" value="{{ $cv->resume_gradate }}" placeholder="From">
			<label for="graduateDateFrom" style="font-size:12px;">Year graduated / left</label>
		@else
			<input  id="graduateDateFrom" name="graduateDateFrom" maxlength='4' type="text" class="validate digits_only" value="{{ old('graduateDateFrom') }}" placeholder="From">
			<label for="graduateDateFrom" style="font-size:12px;">Year graduated / left</label>
		@endif
		@if ($errors->has('graduateDateFrom'))
		<span class="help-block">
			<strong>{{ $errors->first('graduateDateFrom') }}</strong>
		</span>
		@endif
	</div>
	<div class="input-field col s12 m3">
		@if(isset($cv))
			<input  id="graduateDateTo" name="graduateDateTo" maxlength='4' type="text" class="validate digits_only" value="{{ $cv->resume_gradate_to }}"  placeholder="To">
			<label for="graduateDateTo" style="font-size:12px;">Year graduated / left</label>
		@else
			<input  id="graduateDateTo" name="graduateDateTo" maxlength='4' type="text" class="validate digits_only" value="{{ old('graduateDateTo') }}"  placeholder="To">
			<label for="graduateDateTo" style="font-size:12px;">Year graduated / left</label>
		@endif
		@if ($errors->has('graduateDateTo'))
		<span class="help-block">
			<strong>{{ $errors->first('graduateDateTo') }}</strong>
		</span>
		@endif
	</div>
</div>
	


<div class="row">
	<div class="input-field col s12">
		<a class="btn waves-effect waves-light" id="toCertificationInfo">
			NEXT <i class="material-icons right">arrow_forward</i>
		</a>
	</div>
</div>