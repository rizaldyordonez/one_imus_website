<div class="row">
	<div class="input-field col s12 textarea-objective" style="margin-top:30px">
		<label>Work Objective</label>
		<textarea id="work_objective" maxlength="100" name="work_objective" class="materialize-textarea no_special_charNum" style="height:75px!important;">@if(isset($cv)) {{ $cv->resume_objective }} @else {{ old('work_objective') }} @endif</textarea>
		<label for="work_objective"></label>
	</div>
</div>
<div class="row">
	<div class="input-field col s12" id="newJob">
		@if(isset($cv))
		<select name="choose_available_jobs" class="newJobselect" style="">
			<option disabled selected>Choose desired position</option>
			@foreach ($jobs as $value)
				<option @if($value->name == "$value->name") selected @endif >{{ $value->name }}</option>
			@endforeach
		</select>
		@else
		<select name="choose_available_jobs" class="newJobselect">
			<option disabled selected>Choose desired position</option>
			@foreach ($jobs as $value)
				<option @if(old('choose_available_jobs') == "$value->name") selected @endif >{{ $value->name }}</option>
			@endforeach
		</select>
		@endif
	</div>

	<div class="input-field col s12 append-container-prevjobs">
			<h5 style="font-size: 15px;font-weight: 600;">Previous employment (Optional)</h5>
			@if (isset($cv) && !is_null(json_decode($cv->resume_prevjobs)))
				@foreach(json_decode($cv->resume_prevjobs) as $index=>$val)
				<div>
					<div class="input-field col s3">
						<input  id="companyName_{{$index}}" maxlength="20" name="prevDetails[{{$index}}][]" type="text" class="validate no_special_charNum" value="{{ $val[0] }}">
						@if ($errors->has('prevDetails'))
						<span class="help-block">
							<strong>{{ $errors->first('prevDetails') }}</strong>
						</span>
						@endif
						<label for="prevDetails_{{$index}}">Company</label>
					</div>
					<div class="input-field col s3">
						<input  id="job_{{$index}}" maxlength="20" name="prevDetails[{{$index}}][]" type="text" class="validate no_special_charNum" value="{{ $val[1] }}">
						@if ($errors->has('prevDetails'))
						<span class="help-block">
							<strong>{{ $errors->first('prevDetails') }}</strong>
						</span>
						@endif
						<label for="job_{{$index}}">Job title</label>
					</div>
					<div class="input-field col s3">
						<input  id="prev_yearFrom_{{$index}}" name="prevDetails[{{$index}}][]" type="text" maxlength="4" class="validate prevYearfrom digits_only" value="{{ $val[2] }}" placeholder="(Year)">
						@if ($errors->has('prevDetails'))
						<span class="help-block">
							<strong>{{ $errors->first('prevDetails') }}</strong>
						</span>
						@endif
						<label for="prev_yearFrom_{{$index}}">From</label>
					</div>
					<div class="input-field col s3">
						<input  id="prev_yearTo_{{$index}}" name="prevDetails[{{$index}}][]" type="text" maxlength="4" class="validate prevYearTo digits_only" value="{{ $val[3] }}" placeholder="(Year)">
						@if ($errors->has('prevDetails'))
						<span class="help-block">
							<strong>{{ $errors->first('prevDetails') }}</strong>
						</span>
						@endif
						<label for="prev_yearTo_{{$index}}">To</label>
					</div>
				</div>
				@endforeach
			@elseif(old('prevDetails'))
				@foreach(old('prevDetails') as $index => $old_skills)
				<div>
					<div class="input-field col s3">
						<input  id="companyName_{{$index}}" maxlength="20" name="prevDetails[{{$index}}][]" type="text" class="validate no_special_charNum" value="{{ $old_skills[0] }}">
						@if ($errors->has('prevDetails'))
						<span class="help-block">
							<strong>{{ $errors->first('prevDetails') }}</strong>
						</span>
						@endif
						<label for="companyName_{{$index}}">Company</label>
					</div>
					<div class="input-field col s3">
						<input  id="job_{{$index}}" maxlength="20" name="prevDetails[{{$index}}][]" type="text" class="validate no_special_charNum" value="{{ $old_skills[1] }}">
						@if ($errors->has('prevDetails'))
						<span class="help-block">
							<strong>{{ $errors->first('prevDetails') }}</strong>
						</span>
						@endif
						<label for="job_{{$index}}">Job title</label>
					</div>
					<div class="input-field col s3">
						<input  id="prev_yearFrom_{{$index}}" name="prevDetails[{{$index}}][]" type="text" maxlength="4" class="validate prevYearfrom digits_only" value="{{ $old_skills[2] }}">
						@if ($errors->has('prevDetails'))
						<span class="help-block">
							<strong>{{ $errors->first('prevDetails') }}</strong>
						</span>
						@endif
						<label for="prev_yearFrom_{{$index}}">From</label>
					</div>
					<div class="input-field col s3">
						<input  id="prev_yearTo_{{$index}}" name="prevDetails[{{$index}}][]" type="text" maxlength="4" class="validate prevYearTo digits_only" value="{{ $old_skills[3] }}">
						@if ($errors->has('prevDetails'))
						<span class="help-block">
							<strong>{{ $errors->first('prevDetails') }}</strong>
						</span>
						@endif
						<label for="prev_yearTo_{{$index}}">To</label>
					</div>
				</div>
				@endforeach
			@else
			<div>
				<div class="input-field col s3">
					<input  id="companyName_0" maxlength="20" name="prevDetails[0][]" type="text" class="validate no_special_charNum">
					@if ($errors->has('prevDetails'))
					<span class="help-block">
						<strong>{{ $errors->first('prevDetails') }}</strong>
					</span>
					@endif
					<label for="companyName_0">Company</label>
				</div>
				<div class="input-field col s3">
					<input  id="job_0" maxlength="20" name="prevDetails[0][]" type="text" class="validate no_special_charNum">
					@if ($errors->has('prevDetails'))
					<span class="help-block">
						<strong>{{ $errors->first('prevDetails') }}</strong>
					</span>
					@endif
					<label for="job_0">Job title</label>
				</div>
				<div class="input-field col s3">
					<input  id="prev_yearFrom_0" name="prevDetails[0][]" type="text" maxlength="4" class="validate prevYearfrom digits_only" placeholder="(Year)">
					@if ($errors->has('prevDetails'))
					<span class="help-block">
						<strong>{{ $errors->first('prevDetails') }}</strong>
					</span>
					@endif
					<label for="prev_yearFrom_0">From</label>
				</div>
				<div class="input-field col s3">
					<input  id="prev_yearTo_0" name="prevDetails[0][]" type="text" maxlength="4" class="validate prevYearTo digits_only" placeholder="(Year)">
					@if ($errors->has('prevDetails'))
					<span class="help-block">
						<strong>{{ $errors->first('prevDetails') }}</strong>
					</span>
					@endif
					<label for="prev_yearTo_0">To</label>
				</div>
			</div>
			 @endif
	</div>

	<div class="input-field col s12">
		<div class="add-prev-job" style="float:right;">
			<a class="btn waves-effect waves-light append-input-prevjobs">
				ADD <i class="material-icons right">add</i>
			</a>
		</div>
		<div style="clear:both;height:20px;"></div>
		<a class="btn waves-effect waves-light" id="toEducationInfo">
			NEXT <i class="material-icons right">arrow_forward</i>
		</a>
	</div>
	
</div>