<div class="row">
	<div class="input-field col s12 m3">
		@if(isset($cv))
			<input  id="firstname" name="firstname" type="text" maxlength="20" class="validate sentence no_special_charNum" value="{{ $cv->resume_firstname }}">
		@else
			<input  id="firstname" name="firstname" type="text" maxlength="20"class="validate sentence no_special_charNum" value="{{ old('firstname') }}">
		@endif

		@if ($errors->has('firstname'))
		<span class="help-block">
			<strong>{{ $errors->first('firstname') }}</strong>
		</span>
		@endif
		<label for="firstname">First name</label>
	</div>

	<div class="input-field col s12 m3">
		@if(isset($cv))
			<input  id="middlename" name="middlename" type="text" maxlength="20" class="validate sentence no_special_charNum" value="{{ $cv->resume_middlename }}">
		@else
			<input  id="middlename" name="middlename" type="text" maxlength="20" class="validate sentence no_special_charNum" value="{{ old('middlename') }}">
		@endif

		@if ($errors->has('middlename'))
		<span class="help-block">
			<strong>{{ $errors->first('middlename') }}</strong>
		</span>
		@endif
		<label for="middlename">Middle name</label>
	</div>

	<div class="input-field col s12 m3">
		@if(isset($cv))
			<input  id="lastname" name="lastname" type="text" maxlength="20" class="validate sentence no_special_charNum" value="{{ $cv->resume_lastname }}">
		@else
			<input  id="lastname" name="lastname" type="text" maxlength="20" class="validate sentence no_special_charNum" value="{{ old('lastname') }}">
		@endif

		@if ($errors->has('lastname'))
		<span class="help-block">
			<strong>{{ $errors->first('lastname') }}</strong>
		</span>
		@endif
		<label for="lastname">Last name</label>
	</div>

	<div class="input-field col s12 m3">
		@if(isset($cv))
			<input  id="resume_age" name="resume_age" maxlength="2" type="text" class="validate digits_only" value="{{ $cv->resume_age }}">
		@else
			<input  id="resume_age" name="resume_age" maxlength="2" type="text" class="validate digits_only" value="{{ old('resume_age') }}">
		@endif

		@if ($errors->has('resume_age'))
		<span class="help-block">
			<strong>{{ $errors->first('resume_age') }}</strong>
		</span>
		@endif
		<label for="resume_age">Age</label>
	</div>
</div>
<div class="row">

	<div class="input-field col s12 m3">
		@if(isset($cv))
			<input  id="birthdate" name="birthdate" onkeypress="return false;" type="text" class="validate datepicker" value="{{ $cv->resume_birthdate }}">
		@else
			<input  id="birthdate" name="birthdate" onkeypress="return false;" type="text" class="validate datepicker" value="{{ old('birthdate') }}">
		@endif

		@if ($errors->has('birthdate'))
		<span class="help-block">
			<strong>{{ $errors->first('birthdate') }}</strong>
		</span>
		@endif
		<label for="birthdate">Birth date</label>
	</div>

	<div class="input-field col s12 m3">
		@if(isset($cv))
			<input  id="nationality" name="nationality" type="text" maxlength="10" class="validate no_special_charNum" value="{{ $cv->resume_nationality }}">
		@else
			<input  id="nationality" name="nationality" type="text" maxlength="10" class="validate no_special_charNum" value="{{ old('nationality') }}">
		@endif

		@if ($errors->has('nationality'))
		<span class="help-block">
			<strong>{{ $errors->first('nationality') }}</strong>
		</span>
		@endif
		<label for="nationality">Nationality</label>
	</div>

	<div class="input-field col s12 m3">
		<select name="civil_status">
			@if(isset($cv))
				<option value="" disabled selected @if($cv->resume_civil_status == "") selected @endif>Choose Status</option>
				<option @if($cv->resume_civil_status == "Single") selected @endif>Single</option>
				<option @if($cv->resume_civil_status == "Married") selected @endif>Married</option>
				<option @if($cv->resume_civil_status == "Widow/Widower") selected @endif>Widow/Widower</option>
			@else
				<option value="" disabled selected @if(old('civil_status') == "") selected @endif>Choose Status</option>
				<option @if(old('civil_status') == "Single") selected @endif>Single</option>
				<option @if(old('civil_status') == "Married") selected @endif>Married</option>
				<option @if(old('civil_status') == "Widow/Widower") selected @endif>Widow/Widower</option>
			@endif
		</select>
	</div>

	<div class="input-field col s12 m3">
		<select name="gender">
			@if(isset($cv))
				<option value="" disabled selected @if($cv->resume_gender == "") selected @endif>Choose Gender</option>
				<option @if($cv->resume_gender == "Male") selected @endif>Male</option>
				<option @if($cv->resume_gender == "Female") selected @endif>Female</option>
			@else
				<option value="" disabled selected @if(old('gender') == "") selected @endif>Choose Gender</option>
				<option @if(old('gender') == "Male") selected @endif>Male</option>
				<option @if(old('gender') == "Female") selected @endif>Female</option>
			@endif
		</select>
	</div>

	<a class="btn waves-effect waves-light" id="toContactInfo">
		NEXT <i class="material-icons right">arrow_forward</i>
	</a>

</div>