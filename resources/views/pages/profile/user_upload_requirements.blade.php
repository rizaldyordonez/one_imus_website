@extends('layouts.main')
@section('content')

<section class="container">
	<div class="row">
		<h4>Requirements</h4>
		<hr>
		<div class="requirement-container">
		
		<form role="form" method="POST" action="{{ route('doUploadRequirements') }}" enctype="multipart/form-data">
			@csrf
				<div class="file-field input-field">
					<div class="btn">
						<span>Upload</span>
						<input type="file" multiple name="requirements[]">
					</div>
					<div class="file-path-wrapper">
						<input type="hidden" name="username" value="{{ $username }}">
						<input class="file-path validate" type="text" placeholder="Upload one or more requirements">
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<button type="submit" class="btn waves-effect waves-light" id="toSubmit">
							SUBMIT <i class="material-icons right">send</i>
						</button>
					</div>
				</div>
				@if (count($errors) > 0)
				<div class="alert alert-danger error-container">
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif
			
		</form>
		</div>
	</div>
</section>
@endsection

@section('customCSS')
<style type="text/css">
	.requirement-container {
		margin: 0 auto;
		max-width: 600px;
		padding: 200px 0px;
	}
	.error-container {
		padding: 10px 0px;
		background-color: #ff4949;
		border-radius: 10px;
		text-align: center;
		color: #ffff;
	}
</style>
@endsection

@section('customJS')
<script type="text/javascript">
	// $("#drop-area").dmUploader({
	// 	url: 'test',
  
 //  	onInit: function(){
 //  		console.log('Callback: Plugin initialized');
 // 	 }
	// });
</script>
@endsection


{{-- <div id="drop-area">
				<h3>Drag and Drop Files Here<h3/>
					<input id="" type="file" title="Click to add Files">
			</div> --}}
			{{-- <div class="file-field input-field">
				<div class="btn">
					<span>UPLOAD</span>
					<input type="file" id="img-1" name="resume_photo">
				</div>
				<div class="file-path-wrapper">
					<input class="file-path validate" type="text" value="{{old('resume_photo')}}">
				</div>


			</div> --}}