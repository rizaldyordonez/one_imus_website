 <!DOCTYPE html>
<html>
<head>
	<title>Resume PDF</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>

<style type="text/css">
	@page {
		margin: 15px;
	}

	.contain-resume{
		height:990px;
		background-color: #ffffff;
		width:560px;
	}
	.background-profile {
		height: 98%;
		width: 240px;
		background-color: #f3f3f3;
	}
	.profile-left{
		float:left;
	}
	.test-image {
		background-color: grey;
		border-radius: 5px;
		height: 150px;
		width: 150px;
		margin: 0 auto;
	}
	.certificate {
		border-radius: 5px;
		width: 350px;
		height: 350px;
	}
	.box-objective{
		background-color: white;
		width: 200px;
		height: 60px;
		margin: 0 auto;
	}
	.center {
		text-align:center;
	}
	#right-content h3{
		border-bottom: 2px solid;
		width: 500px;
	}

	#certificate h3{
		border-bottom: 2px solid;
		width: 500px;
	}

	#right-content .content, #certificate .content{
		margin: 10 10px;
		word-wrap: break-word;
	}

	#right-content .content table>tbody>tr>td {
		padding: 5px 5px;
		word-wrap: break-word;
	}

	#right-content .reference p{
		margin: 0;
	}
	.clearboth{
		clear: both;
		height: 10px;
	}
	.clearheight{
		height: 30px;
	}
	body {
		font-size: 12.5px;
	}
	.next-page{
		height:10px;
		clear:both;
	}
	.page-break {
		page-break-after: always;
	}
	table td.adjust {
		width: 100%;
	}
</style>
<body>
	<div class="contain-resume">
		<div class="background-profile profile-left">
			<div class="clearboth"></div>
			<div class="test-image">
				<div class="">
					<img src="../public/images/picture/{{ $resume->resume_picture }}" class="test-image">
					{{-- <img src="../public_html/images/picture/{{ $resume->resume_picture }}" class="test-image"> --}}
				</div>
			</div>
			@php $middle = substr($resume->resume_middlename,0,1);
				 $middle = ucwords($middle.".");
			@endphp
			<div class="center"><h3>{{ ucwords($resume->resume_firstname).' '.$middle.' '.ucwords($resume->resume_lastname) }}</h3></div>
			<div class="clearboth"></div>
			<div class="box-objective">
				<div class="center" >
					<h4>Objective</h4>
				</div>
			</div>
			<div class="center" style="padding: 0 5px;">
				<p>
					{{ ucwords($resume->resume_objective) }}
				</p>
			</div>
		</div>

		<div class="main-content profile-left" id="right-content">
			<div class="content">
				<h3>{{ "POSITION DESIRED" }}</h3>
				<table class="striped">
					<tbody>
						<tr>
							<td>Position desired</td>
							<td>:</td>
							<td>{{ $resume->resume_newjob }}</td>
						</tr>
					</tbody>
				</table>
			</div>
			{{-- Personal Information --}}
			<div class="content">
				<h3>{{ "PERSONAL INFORMATION" }}</h3>

				<table class="">
					<tbody>
						<tr>
							<td><b>Birthdate</b></td>
							<td>:</td>
							<td>{{ $resume->resume_birthdate }}</td>
							<td> </td>
							<td><b>Gender</b></td>
							<td>:</td>
							<td>{{ ucwords($resume->resume_gender) }}</td>
						</tr>
						<tr>
							<td><b>Birth place</b></td>
							<td>:</td>
							<td>{{ $resume->resume_city }}</td>
							<td> </td>
							<td><b>Civil Status</b></td>
							<td>:</td>
							<td>{{ ucwords($resume->resume_civil_status) }}</td>
						</tr>
						<tr>
							<td><b>Age</b></td>
							<td>:</td>
							<td>{{ $resume->resume_age }}</td>
							<td> </td>
							<td><b>Address</b></td>
							<td>:</td>
							<td>{{ ucwords($resume->resume_homeaddress.', '.ucwords($resume->resume_city)) }}</td>
						</tr>
					</tbody>
				</table>
				<table>
					<tbody>
						<tr>
							<td colspan="2"><b>Nationality</b></td>
							<td>:</td>
							<td>{{ ucwords($resume->resume_nationality) }}</td>
						</tr>
					</tbody>
				</table>
			</div>
			{{-- End of Personal Information --}}

			{{-- Contact Information --}}
			<div class="content">
				<h3>{{ "CONTACT INFORMATION" }}</h3>
				<table class="striped">
					<tbody>
						<tr>
							<td><b>Mobile</b></td>
							<td>:</td>
							<td>{{ (empty($resume->resume_mobilenumber)? "----": $resume->resume_mobilenumber) }}</td>
							<td> </td>
							<td><b>E-mail</b></td>
							<td>:</td>
							<td>{{ (empty($resume->resume_email)? "----": $resume->resume_email) }}</td>
						</tr>
					</tbody>
				</table>
			</div>
			{{-- End of Contact Information --}}

			{{-- Skills --}}
			<div class="content">
				<h3>{{ "SKILLS" }}</h3>
				@php
					
					$skills = json_decode($resume->resume_skills, true);
					$skills_name = $skills['name'];
					$count_skills = count($skills_name);
				@endphp
				<ul>
					@foreach($skills_name as $index=>$val)
					<li>({{ $skills['year'][$index]." Year/s" }}) - {{ ucwords($val) }}</li>
					@endforeach
				</ul>
			</div>
			{{-- End Skills --}}

			{{-- ACHIEVEMENTS --}}
			@if(!is_null($resume->resume_achievement))
			@php
				$count_achievements = count(json_decode($resume->resume_achievement, true));
			@endphp
			<div class="content">
				<h3>{{ 'ACHIEVEMENTS' }}</h3>
				<ul>
					@foreach (json_decode($resume->resume_achievement) as $index=>$val)
					@for ($i = 0; $i < sizeof($val); $i++)
						<li>{{ $val[$i] }}</li>
					@endfor
					@endforeach
				</ul>
			</div>
			@endif
			{{-- END OF ACHIEVEMENTS --}}

			{{-- Work Experience --}}
			@if (!is_null($resume->resume_prevjobs))
				@php
				$count_prevjobs = count(json_decode($resume->resume_prevjobs, true));
				if (isset($count_prevjobs) && !is_null($count_prevJobs)) {
					$count_prevjobs = $count_prevjobs;
				}else{
					$count_prevjobs = 0;
				}
				@endphp
				@if(count(json_decode($resume->resume_prevjobs))>4)
					<div style="clear:both;height:10px;"></div>
				@endif
			<div class="content">
				<h3>{{ "WORK EXPERIENCE" }}</h3>
				<table class="striped">
					<tbody>
						@foreach(json_decode($resume->resume_prevjobs) as $index=>$val)
						<tr>
							@for($i = 0; $i<sizeof($val); $i++)
							<td>
								@if ($i=='2')
								    {{ $val[$i] }} <td width="10">to</td>
								@else
									{{ $val[$i] }}
								@endif
							</td>
							@endfor
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			@endif

			{{-- End of Work Experience --}}

			{{-- Education Information --}}
			<div class="content">
				<h3>{{ "EDUCATION" }}</h3>
				<table class="striped">
					<tbody>
						<tr>
							<td>{{ $resume->resume_gradate }}-{{ $resume->resume_gradate_to }}</td>
							<td>:</td>
							<td><b>{{ ucwords($resume->resume_school) }}</b></td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td>{{ ucwords($resume->resume_edu_attain) }}</td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td>{{ ucwords($resume->resume_field_study) }}</td>
						</tr>
					</tbody>
				</table>
			</div>
			
			{{-- References Information --}}
			{{-- @if($count_achievements>3 && $count_prevjobs>3 && $count_skills>3)
			<div style="clear:both;height:15px;"></div>
			@endif --}}
			<div class="content">
				<h3>{{ "REFERENCES" }}</h3>
				<table class="striped">
					<tbody>
						@php
							$references = json_decode($resume->resume_references, true);
							$references_name = $references['ref_name'];
						@endphp
						@foreach($references_name as $index=>$val)
						<tr>
							<td width="50">{{ ucwords($val) }}</td>
							<td width="50">{{ ucwords($references['ref_relation'][$index]) }}</td>
							<td width="50">{{ ucwords($references['ref_company'][$index]) }}</td>
							<td width="50">{{ ucwords($references['ref_position'][$index]) }}</td>
							<td width="50">{{ ucwords($references['ref_phone'][$index]) }}</td>
							<td width="50">{{ ucwords($references['ref_email'][$index]) }}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			{{-- End of References Information --}}
		</div>
			{{-- MOVING TO NEXT PAGE STARTS HERE --}}

			{{-- Certificates --}}
			@if(!empty($resume->resume_certification))
			<div class="next-page"></div>
			<div id="certificate">
				<div class="content">
					<h3>{{ "CERTIFICATION" }}</h3>
					@foreach(json_decode($resume->resume_certification) as $index=>$val)
					<div class="clearheight"></div>
					<div class="certificate">
						<div class="">
							<img src="../public/images/certificates/{{ $val }}" class="certificate">
							
							{{-- <img src="../public_html/images/certificates/{{ $val }}" class="certificate"> --}}
						</div>
						<div class="clearboth"></div>
					</div>
					@endforeach
				</div>
				@endif
			</div>
			{{-- End of Certificates --}}
	</div>
</body>
</html>