@extends('layouts.main')
@section('content')

<section class="container">
	<div class="col s12 m12">
		<div class="row">
			<div class="col s6 m5">
				<div class="left"><h2> PROFILE </h2></div>
				<div class="clear_both_1"></div>
				<div>{{ strtoupper(Auth::user()->name) }}</div>
				<div class="clear_both_1"></div>
				<div>{{ Auth::user()->email }}</div>
				<div class="clear_both_1"></div>
				<div class="col s12 m12 no-padding">
					<div class="col s6 m5 no-padding">
						<a class="waves-effect waves-light btn modal-trigger" href="#modal2">Change Email</a>
					</div>
					<div class="col s6 m6 no-padding">
						<!-- Modal Trigger -->
						<a class="waves-effect waves-light btn modal-trigger" href="#modal1">Change Password</a>
					</div>
				</div>
				
			</div>
			<div class="col s6 m7">
				@if(!isset($cv) && !isset($application) && $verified)
					<div class="right"><h4><a href="{{ route('showInsertResume') }}" class="btn btn-primary">ADD CV</a></h4></div>
				@elseif(isset($cv) && !isset($application))
				<div class="right profile-btn-right"><h4><a href="{{ route('showInsertResume') }}" class="btn btn-primary">EDIT CV</a></h4></div>
				<div class="right profile-btn-right"><h4><a href="{{ route('showPDF') }}" target="_blank" class="btn btn-primary">VIEW CV</a></h4></div>
				<div class="right profile-btn-right  user_{{ Auth::user()->id }}" id="data-name-{{ Auth::user()->name }}"><h4><a href="{{ route('showAllJobs', ['true']) }}" class="btn btn-primary modal-trigger">GO TO JOBS</a></h4></div>
				@elseif(isset($cv) && isset($application))
				<div class="right profile-btn-right"><h4><a href="{{ route('showInsertResume') }}" class="btn btn-primary">EDIT CV</a></h4></div>
				<div class="right profile-btn-right"><h4><a href="{{ route('showPDF') }}" target="_blank" class="btn btn-primary">VIEW CV</a></h4></div>
					@if($application->stage == 3)
					@if(Session::has('success'))
					<div class="alert alert-success left success-req">
						{{ Session::get('success') }}
					</div>
					@endif
						<div class="right profile-btn-right"><h4><a href="{{ route('showUploadRequirements') }}" class="btn btn-primary">UPLOAD REQUIREMENTS</a></h4></div>
					@endif
				@endif
				@if (!$verified)
					<div class="right profile-btn-right"><h4><a href="{{ route('reverifyMail') }}" class="btn btn-primary">RESEND EMAIL VERIFICATION</a></h4></div>
				@endif
				
				<div class="clear_both"></div>
				<div>
					@if(isset($application) && $application->stage == 0)
						<span>Your application is being reviewed</span>
					@elseif(isset($application) && $application->stage == 1)
						<span>You status is on Initial Interview</span>
					@elseif(isset($application) && $application->stage == 2)
						<span>You status is on Final Interview</span>
					@elseif(isset($application) && $application->stage == 3)
						<span>You status is on Requirements</span>
					@else
					@endif
				</div>
			</div>
		</div>
	</div>
	<div class="col s12 m12">
		<div class="row">
			<div class="col s6 m6">
				<table class="striped">
					<thead>
						<tr>
							<th>Activity Log</th>
							<th>Date / Time</th>
						</tr>
					</thead>

					<tbody class="table-body">
						@foreach($logs as $log)
						<tr>
							<td>{{ $log->activity }}</td>
							<td>{{$log->created_at->format("M-d-Y / h:i:s A")}}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="col s6 m6">
				<table class="striped">
					<thead>
						<tr>
							<th>CV Activity</th>
							<th>Date / Time</th>
						</tr>
					</thead>

					<tbody>
						@foreach($resume_logs as $resume_log)
						<tr>
							<td>{{ $resume_log->activity }}</td>
							<td>{{$resume_log->created_at->format("M-d-Y / h:i:s A")}}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
	
	</section>
  <!-- Modal Structure -->
  <div id="modal1" class="modal form-modal">
  	<form method="POST" action="" id="form_pass" enctype="multipart/form-data" onsubmit="changePass(); return false;">
  		<div class="modal-content">
  			<h5>Change Password</h5>

  			<div class="row">
                <div class="input-field col s12">
                  <input  id="password" name="old_password" type="password" class="validate" required>
                  @if ($errors->has('old_password'))
                  <span class="help-block">
                    <strong>{{ $errors->first('old_password') }}</strong>
                  </span>
                  @endif
                  <label for="password">Current Password</label>
                </div>
              </div>

  			<div class="row">
                <div class="input-field col s12">
                  <input  id="password" name="password" type="password" class="validate" required>
                  @if ($errors->has('password'))
                  <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                  </span>
                  @endif
                  <label for="password">New Password</label>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                  <input  id="confirm_password" name="password_confirmation" type="password" class="validate" required>
                  @if ($errors->has('password_confirmation'))
                  <span class="help-block">
                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                  </span>
                  @endif
                  <label for="confirm_password">Confirm New Password</label>
                </div>
              </div>

  			<div class="modal-footer">
  			<div id="message" class="message">
  			</div>
  			<a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
  			<button class="btn waves-effect waves-light" type="submit" name="action" id="change_pass_btn">Submit
  				<i class="material-icons right">send</i>
  			</button>
  		</div>

  		</div>
  	</form>
  </div>

  <!-- Modal Structure -->
  <div id="modal2" class="modal form-modal">
  	<form method="POST" action="" id="form_email" enctype="multipart/form-data" onsubmit="changeEmail(); return false;">
  		<div class="modal-content">
  			<h5>Change Email</h5> <span class="note">NOTE: You must verify your entered email before we change it to your current email.</span>

  			<div class="row">
                <div class="input-field col s12">
                  <input  id="email" name="email" type="email" class="validate" required>
                  @if ($errors->has('email'))
                  <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                  </span>
                  @endif
                  <label for="password">New Email</label>
                </div>
              </div>

  			<div class="modal-footer">
  			<div id="email-message" class="message">
  			</div>
  			<a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
  			<button class="btn waves-effect waves-light" type="submit" name="action" id="change_email_btn">Submit
  				<i class="material-icons right">send</i>
  			</button>
  		</div>

  		</div>
  	</form>
  </div>

  <div id="modal2" class="modal modal-ask form-modal">
  	<div class="modal-content">
  		<h5>Submit Resume</h5>

  		<span>Are you sure to submit your resume?</span>

  		<div id="message" class="message">
  		</div>
  		<a href="" class="modal-close waves-effect waves-green btn-flat">No</a>
  		<a class="btn waves-effect waves-light" id="submitCV_btn" onClick="submitCV( ['{{ Auth::user()->id }}','{{ Auth::user()->name }}'] )">Yes
  			<i class="material-icons right">send</i>
  		</a>
  	</div>
  </div>
  	
  </div>
@endsection

@section('customCSS')
<style type="text/css">
	.table-body{
		overflow-x: scroll;
		height: 312px;
	}
	.form-modal{
		min-width: 40%;
	}
	.message{
		float: left;
	}
	.no-padding{
		padding: 0px;
	}
	.note{
		color: #ef8601;
		font-size: 12px;
	}
</style>
@endsection

@section('customJS')
<script type="text/javascript">
	function changePass()
	{
		var data = $('#form_pass').serialize();

		$.ajax({
			headers: {
				'X-CSRF-TOKEN':  $('meta[name="csrf-token"]').attr('content')
			},
			type: "POST",
			dataType: 'json',
			data: data,
			url: "{{ route('changePassword') }}", 
			success: function(data) {
				$('#message').text('');
				if(data.success == true){
					$('#message').css('color','green').text(data.message);
					setTimeout(function(){ location.reload(); }, 1000);
				}else{
					$('#message').css('color','red').text(data.message);
					$( "#change_pass_btn" ).prop( "disabled", false );
				}
			},
			error: function() {
				alert("error");
			}
		});
	}

	function changeEmail()
	{
		var data = $('#form_email').serialize();

		$.ajax({
			headers: {
				'X-CSRF-TOKEN':  $('meta[name="csrf-token"]').attr('content')
			},
			type: "POST",
			dataType: 'json',
			data: data,
			url: "{{ route('changeMail') }}", 
			success: function(data) {
				$('#message').text('');
				if(data.success == true){
					$('#email-message').css('color','green').text(data.message);
					setTimeout(function(){ location.reload(); }, 1000);
				}else{
					$('#email-message').css('color','red').text(data.message);
					$( "#change_email_btn" ).prop( "disabled", false );
				}
			},
			error: function() {
				alert("error");
			}
		});
	}

	function submitCV(array)
	{

		var obj = {"userid":array[0],"username":array[1],"_token":"{{ csrf_token() }}"};
		var name = array[1];
		var id = array[0];

		$("#spin-ld-"+id).show();
		$('.modal').modal('close');
		$.ajax({
			type: "POST",
			url: "{{ route('doSubmitResume') }}",
			data: obj,
			success: function(data) {
				if (data.success) {

					location.reload();
					alert("successfully submitted");
					
				} else {
					alert(data.error);
				}
			},
			error: function() {
				alert("error");
			}
		});
		
		$("#spin-ld-"+id).hide();
	}
</script>
@endsection