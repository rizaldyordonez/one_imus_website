@extends('layouts.main')
@section('customCSS')
<style type="text/css">
	.card {
		position: relative;
		margin: .5rem 0 1rem 0;
		background-color: #4572cf;
		color: #fff;
		-webkit-transition: -webkit-box-shadow .25s;
		transition: -webkit-box-shadow .25s;
		transition: box-shadow .25s;
		transition: box-shadow .25s, -webkit-box-shadow .25s;
		border-radius: 2px;
	}

	p {
		font-family: sans-serif;
	}

	a.job-category-href {
		text-decoration: underline;
	}

	.card-description {
		background: #f7f7f7;
	}

	ul li.description-list {
		list-style-type: initial; 
	}

	.breadcrumb:before {
		color: #fb8c00;
	}

	.breadcrumb, .breadcrumb:last-child {
		color: #000;
	}
	
	.section-breadcrumb {
		margin: 0 auto;
		width: 98%;
		font-size: 21px;
	}

	.text-black {
		color: black !important;
	}

	.description-container {
		max-height: 500px;
		height: 500px;
		overflow-y: scroll;
	}

	.parallax-container {
		height: 100%!important;
	}

	.table-body{
		overflow-x: scroll;
		height: 312px;
	}
	.form-modal{
		min-width: 40%;
	}
	.message{
		float: left;
	}

</style>
@endsection
@section('content')
<section class="parallax-container section scrollspy" id="">
	<div class="container">
		<div class="col s12 m12">
			<div class="row">
				<div class="col s12 m12 l12">
					<div class="section">
						<div class="section-breadcrumb">
							<a href="{{ route('showJobs', ['job_category'=>$job_category]) }}" @if($info===true) class='breadcrumb text-black' @else class='text-black' @endif  ">{{ ucwords($job_category) }}</a>
							<a href="#"  @if($info===true) class='breadcrumb text-black' @else class='text-black' @endif>{{ ucwords($job_category_list) }}</a>
						</div>
					</div>
					<div class="divider"></div>
					<div class="clear_both"></div>
				</div>
				<div class="col s12 m12 l12">
					@if ($info===true)
					<h6>{{ ucwords($job_category_list) }} details</h6>
						@foreach ($available_jobs as $index=>$value)
						<div class="col s12 m12">
							<div class="text-black ">
								{!! $value->description !!}
							</div>
							<div class="clear_both"></div>
						</div>
						
						@endforeach
						@if (!Auth::check())
							<div class="left profile-btn-right"><h4><a href="{{ route('login') }}" class="btn btn-primary">Login</a></h4></div>
						@else
							@if (!$hasActiveResume)
								@if ($verified>0)
									<div class="left wow fadeInLeft profile-btn-right user_{{ $username }}" id="data-name-{{ $id }}"><h4><a href="#modal2" class="btn btn-primary modal-trigger">{{ $resume_status }}</a></h4></div>
								@else
									<p>Email not yet verified. Please verify first.</p>
									<div class="left wow shake profile-btn-right"><a href="{{ route('reverifyMail') }}" class="btn btn-primary modal-trigger">Resend E-mail Verification</a></div>
									{{-- <div class="left profile-btn-right user_{{ $username }}" id="data-name-{{ $id }}"><h4><a href="#modal2" class="btn btn-primary modal-trigger">SUBMIT CV</a></h4></div> --}}
									<div></div>
								@endif
							@else
								<div class="left wow shake profile-btn-right user_{{ $username }}" id="data-name-{{ $id }}"><h4><a href="" class="btn btn-primary disabled">{{ $resume_status }}</a></h4></div>
							@endif
						@endif
						
					@else
						@foreach ($available_jobs as $index=>$value)
						{{-- {{dd($value)}} --}}
						<div class="col s12 m4">
							<div class="card hoverable hvr-bounce-to-bottom">
								<div class="card-content">
									<h6>{{ ucwords($value->name) }} </h6>
									<div class="right"><span class="new badge">{{ $value->jobs_number }}</span></div>
									<h6><a id="#{{ strtolower($value->name) }}" href="{{ route('showJobs', ['job_category'=>$value->job_category,
									'job_category_list'=>$value->name, 'partner_id'=>$value->partner_id, 'job_id'=>$value->id]) }}" class="see-full-details">
										See full details <i class="fa fa-chevron-circle-right hvr-icon"></i> </a></h6>
									</div>
								</div>
							</div>
							{{-- <li>
								<div class="collapsible-header"><i class="material-icons">arrow_drop_down_circle</i>{{ ucwords($value->name) }}</div>
								<div class="collapsible-body">
									<span>{!! $value->description !!}</span>
									<a href="#" class="job-category-href right">Send CV</a>
								</div>

							</li> --}}
						@endforeach
					@endif
				</div>
				
			</div>
		</div>
	</div>

	@if ($info===true)
	<div id="modal2" class="modal modal-ask form-modal">
		<div class="modal-content">
			<h5>Submit Resume</h5>

			<span>Are you sure to submit your resume?</span>

			<div id="message" class="message">
			</div>
			<a href="" class="modal-close waves-effect waves-green btn-flat">No</a>
			<a class="btn waves-effect waves-light" id="submitCV_btn" onClick="submitCV( ['{{ $id }}','{{ $username }}','{{ $job_id }}'] )">Yes
				<i class="material-icons right">send</i>
			</a>
		</div>
	</div>
	@endif
</section>
@endsection

@section('customJS')
<script type="text/javascript">
	
	function submitCV(array)
	{
		var obj = {"userid":array[0],"username":array[1],"job_id":array[2],"_token":"{{ csrf_token() }}"};
		
		var id = array[0];
		var name = array[1];
		var job_id = array[2];

		$("#spin-ld-"+id).show();
		$('.modal').modal('close');
		$.ajax({
			type: "POST",
			url: "{{ route('doSubmitResume') }}",
			data: obj,
			success: function(data) {
				if (data.success) {

					location.reload();
					alert("Successfully submitted!");
					
				} else {
					alert(data.error);
				}
			},
			error: function() {
				alert("error");
			}
		});
		
		$("#spin-ld-"+id).hide();
	}


</script>
@endsection