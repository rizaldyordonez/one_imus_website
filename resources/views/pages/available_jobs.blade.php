<div class="parallax-container about_us_bg section scrollspy" id="available_jobs">
	<div class="container">
		<div class="col s12 m12">
			<div class="row">
				<div class="col s12 m12 l12">
					<div class="section">
						<h4>Our Jobs</h4>
					</div>
					<div class="divider"></div>
					<div class="clear_both"></div>
				</div>

				@foreach ($available_jobs as $index=>$value)
				<div class="col s12 m4">
					<div class="card hoverable hvr-bounce-to-bottom @if($index>5) overlay-disabled @endif">
						<div class="card-content">
							<h6>{{ ucwords($value->job_category) }} </h6>
							<h6><a id="#{{ strtolower($value->job_category) }}" href="{{ route('showJobs', ['job_category'=> strtolower($value->job_category), 'partner_id'=> $value->partner_id]) }}" class="see-more">See more <i class="fa fa-chevron-circle-right hvr-icon"></i> </a></h6>
						</div>
					</div>
				</div>
				@endforeach
			</div>
			<div class="row">
				<div class="col s12 m12">
					@if(count($available_jobs)>3)
					<div class="show-see-more">
						<a href="{{ route('showAllJobs', ['all_jobs'=> true]) }}" class="see-full-list">
							<h5>See full list here</h5>
						</a>
					</div>
					@endif
				</div>
			</div>
		</div>
	</div>
	<div class="parallax">
		<img src="{{ asset('images/parallax_image/sky_001.jpg') }}">
	</div>
</div>