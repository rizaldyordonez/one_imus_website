<p>
	<p> Hi, {{ $name }} </p>

	<p>This is ONEIMUS Job Contracting Services!</p>

	<p>To confirm your request to change your email? Please click <a href="{{ route('verifyMail', ['email' => $email, 'code' => $code]) }}" target="_blank"> here </a>.</p> 

	<p>NOTE: This email is auto-generated email. You must not reply to this email.</p>

	<p>Best Regards,</p>

	<p>Oneimus team</p>
</p>