@extends('layouts.main')

@section('customCSS')
<style type="text/css">

section {
	height: 100vh;
}

/*iPhone 5 to 8 Plus PORTRAIT*/
@media only screen 
  and (min-device-width: 320px) 
  and (max-device-width: 420px)
  and (-webkit-min-device-pixel-ratio: 2)
  and (orientation: portrait) {
  	section {
  		height: 100% !important;
  	}
  	.parallax-container {
  		height: 100% !important;
  	}
  	.carousel {
  		height: 20vh !important;
  	}
  	.carousel.carousel-slider .carousel-item h2 {
  		font-size: 15px;
  		font-weight: 500;
  		background-color: #fb8c0070;
  		width: 50%;
  		top:30px!important;
  	}
  }


@media only screen 
  and (min-device-width: 320px) 
  and (max-device-width: 568px)
  and (-webkit-min-device-pixel-ratio: 2)
  and (orientation: landscape) {
  	section {
  		height: 100% !important;
  	}
  	.parallax-container {
  		height: 100% !important;
  	}
  }
	
	/*.carousel {
		height: auto !important;
	}*/

</style>
@endsection

@section('content')

{{-- SLIDER --}}
@include('pages.slider')
{{-- END OF SLIDER --}}

{{-- AVAILABLE JOBS --}}
@include('pages.available_jobs')
{{-- END OF AVAILABLE JOBS --}}
{{-- ABOUT US --}}
@include('pages.about_us')
{{-- END OF ABOUT US --}}

{{-- CONTACT US --}}
@include('pages.contact_us')
{{-- END OF CONTACT US --}}

{{-- LOGIN MODAL --}}
@include('pages.auth.login_modal')
{{-- END OF LOGIN MODAL --}}

@endsection

@section('customJS')
<script type="text/javascript">
	$(document).ready(function(){
    clearForm();
		// var current = location.pathname;
		// $('.navbar-fix li').each(function(){
		// 	var zthis = $(this);
  //       // if the current path is like this link, make it active
  //       if(zthis.attr('href').indexOf(current) !== -1){
  //       	zthis.addClass('active');
  //       }

	});

  function clearForm()
  {
    $('#contact-form').val("");
  }

	function sendContact()
	{
		var data = $('#contact-form').serialize();
    $("#contact_us_btn").addClass("disabled");
    
		$.ajax({
			headers: {
				'X-CSRF-TOKEN':  $('meta[name="csrf-token"]').attr('content')
			},
			type: "POST",
			dataType: 'json',
			data: data,
			url: "{{ route('sendMail') }}", 
			success: function(data) {
				if(data.success){
					$("#contactus_message").text(data.message).css("color","green");
					$("#contact_us_btn").removeClass("disabled");
          $("#email").val("");
          $("#textarea1").val("");
          setTimeout(function(){ location.reload(); }, 1000);

				}else{
					$("#contactus_message").text(data.message).css("color","red");
          $("#email").val("");
          $("#textarea1").val("");
          $("#contact_us_btn").removeClass("disabled");
				}
				
			},
			error: function() {
				alert("error");
			}


		});
	}
</script>
@endsection