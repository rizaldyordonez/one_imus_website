@extends('layouts.main')
@section('customCSS')
<style type="text/css">

  .login_padding {
    padding: 100px;
  }

  .panel-heading {
    font-weight: bold;
    font-size: 20px;
  }

  .login_height {
    height: max-content;
  }

  .login_bg {
    background: url({{ asset('images/pages/login_bg_02.jpeg') }}) no-repeat center center fixed;
    background-size: cover;
    background-position-y: 60%;
  }

  .login_test{
    background-color: #000;
    max-height: 
  }

  .distance-height {
    height: 10px;
  }

  .card .card-content {
    padding: 100px;
    border-radius: 0 0 2px 2px;
}
     
</style>
@endsection

@section('content')

<div class="parallax-container login_bg">
  <div class="distance-height"></div>
  <div class="container">
    <div class="row">
      <div class="col s12 m3"></div>
      <div class="col s12 m6">
        <div class="card hoverable">
          <div class="card-content white-text">
            <span class="card-title card-title-black">Reset password</span>

            <form id="login-form" role="form" method="POST" action="{{ url('/do-submit-verification') }}">
              {{ csrf_field() }}
              <div class="row">
                <div class="input-field col s12">
                  <input  id="e-mail" name="email" type="email" class="validate">
                  @if ($errors->has('email'))
                  <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                  </span>
                  @endif
                  <label for="e-mail">E-mail</label>
                  <span class="helper-text" data-error="wrong" data-success="right">E-mail address only</span>

                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                  <input  id="e-mail" name="user_name" type="text" class="validate">
                  @if ($errors->has('user_name'))
                  <span class="help-block">
                    <strong>{{ $errors->first('user_name') }}</strong>
                  </span>
                  @endif
                  <label for="user_name">Registered name</label>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                  <input  id="contact" name="contact" type="text" maxlength="11" class="validate">
                  @if ($errors->has('contact'))
                  <span class="help-block">
                    <strong>{{ $errors->first('contact') }}</strong>
                  </span>
                  @endif
                  <label for="contact">Contact number</label>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                  <button type="submit" class="btn btn-primary">
                    <i class="fa fa-btn fa-sign-in"></i> Submit Verification
                  </button>
                </div>
              </div>
            </form>
          </div>
      </div>
      <div class="col s12 m3"></div>
    </div>
  <div class="col s12">
    

    @if(Session::get("msg_error"))
    <div class="alert alert-danger text-center">
      {{Session::get("msg_error")}}
    </div>
    @endif
   
  </div>
</div>
</div>
</div>
@endsection