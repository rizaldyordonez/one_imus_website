<!-- Modal Structure -->
<div id="modal_login" class="modal">
  <div class="modal-content">
    <div class="row">
      <div class="col s12">
        <form>
        <div class="col s6">
          <div class="row">
            <div class="input-field col s12">
              <input  id="e-mail" name="email" type="email" class="validate">
              <label for="e-mail">E-mail</label>
              <span class="helper-text" data-error="wrong" data-success="right">E-mail address only</span>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <input  id="password" name="password" type="password" class="validate">
              <label for="password">Password</label>
            </div>
          </div>
        </div>
        <a href="javascript:void(0);" onclick="doLogin()" class="modal-close waves-effect waves-green btn">Agree</a>
        </form>
      </div>
    </div>
  </div>
  <div class="modal-footer">

    <span>Don't have an account? </span><a style="text-decoration: underline;" href="#" class="modal-close waves-effect waves-green btn-flat">Register</a>
    <a href="#" class="modal-close waves-effect waves-green btn">Agree</a>
  </div>
</div>

{{-- {{ !!       <div class="col s6 m6">
        <img src="{{ asset('images/HD/blank_note_pen.jpg') }}">
      </div>
      <form class="col s6 m6 offset-m6 l6 offset-l6">

        <div class="row">
          <div class="input-field col s12">
            <input placeholder="Email" id="email" type="email" class="validate">
            <label for="email"></label>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <input placeholder="Password" id="password" type="password" class="validate">
     <label for="password">Password</label>
          </div>
        </div>
        
       <div class="row">
          <div class="col s12">
            This is an inline input field:
            <div class="input-field inline">
              <input id="email_inline" type="email" class="validate">
              <label for="email_inline">Email</label>
              <span class="helper-text" data-error="wrong" data-success="right">Helper text</span>
            </div>
          </div>
        </div>
        !! }} --}}