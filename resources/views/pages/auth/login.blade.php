@extends('layouts.main')
@section('customCSS')
<style type="text/css">

  .login_padding {
    padding: 100px;
  }

  .panel-heading {
    font-weight: bold;
    font-size: 20px;
  }

  .login_height {
    height: max-content;
  }

  .login_bg {
    background: url({{ asset('images/pages/login_bg_02.jpeg') }}) no-repeat center center fixed;
    background-size: cover;
    background-position-y: 60%;
    height: 43.9rem;
  }

  .login_test{
    background-color: #000;
    max-height: 
  }

  .distance-height {
    height: 10px;
  }

  .danger{
    color: #fff;
    font-size: 14px;
    font-weight: bold;
    background-color: orange;
    height: 35px;
    width: 100%;
    border-radius: 5px;
    text-align: center;
    margin: 0;
    padding: 5px;
  }
     
</style>
@endsection

@section('content')

<div class="parallax-container login_bg">
  <div class="distance-height"></div>
  <div class="container">
    <div class="row">
      <div class="col s12 m3"></div>
      <div class="col s12 m6" {{-- style="margin: 7.2rem 0;" --}}>
        <div class="card hoverable">
          <div class="card-content white-text">
            <span class="card-title card-title-black">Please login</span>

            <form id="login-form" role="form" method="POST" action="{{ url('/do-login') }}">
              {{ csrf_field() }}
              <div class="row">
                <div class="input-field col s12">
                  <input  id="e-mail" name="email" type="email" class="validate">
                  @if ($errors->has('email'))
                  <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                  </span>
                  @endif
                  <label for="e-mail">E-mail</label>
                  <span class="helper-text" data-error="wrong" data-success="right">E-mail address only</span>

                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                  <input  id="password" name="password" type="password" class="validate">
                  @if ($errors->has('password'))
                  <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                  </span>
                  @endif
                  <label for="password">Password</label>
                </div>
              </div>
              <div class="row">
                <div class="col s12">
                <a href="{{route('showForgotPassword')}}">Forgot password?</a>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                  <button type="submit" class="btn btn-primary">
                    <i class="fa fa-btn fa-sign-in"></i> Login
                  </button>
                </div>
              </div>
            </form>
            <div class="row">
              @if(Session::get("message"))
              <div class="danger text-center">
                {{Session::get("message")}}
              </div>
              @endif
            </div>
          </div>
      </div>
      <div class="col s12 m3"></div>
    </div>
  <div class="col s12">
    

    @if(Session::get("msg_error"))
    <div class="alert alert-danger text-center">
      {{Session::get("msg_error")}}
    </div>
    @endif
   
  </div>
</div>
</div>
</div>
@endsection