@extends('layouts.main')
@section('customCSS')
<style type="text/css">

  .login_padding {
    padding: 100px;
  }

  .panel-heading {
    font-weight: bold;
    font-size: 20px;
  }

  .login_height {
    height: max-content;
  }

  .login_bg {
    background: url({{ asset('images/pages/login_bg_02.jpeg') }}) no-repeat center center fixed;
    background-size: cover;
    background-position-y: 60%;
  }

  .login_test{
    background-color: #000;
    max-height: 
  }

  .distance-height {
    height: 10px;
  }

  .card .card-content {
    padding: 100px;
    border-radius: 0 0 2px 2px;
 }

 .danger{
  color: #fff;
  font-size: 14px;
  font-weight: bold;
  background-color: orange;
  height: 35px;
  width: 100%;
  border-radius: 5px;
  text-align: center;
  margin: 0;
  padding: 5px;
 }
     
</style>
@endsection

@section('content')

<div class="parallax-container login_bg">
  <div class="distance-height"></div>
  <div class="container">
    <div class="row">
      <div class="col s12 m3"></div>
      <div class="col s12 m6">
        <div class="card hoverable">
          <div class="card-content white-text">
            <span class="card-title card-title-black">Type your new password</span>

            <form id="login-form" role="form" method="POST" action="{{ url('/do-change-password') }}">
              {{ csrf_field() }}

              <div class="row">
                <div class="input-field col s12">
                  <input  id="password" name="password" type="password" class="validate">
                  @if ($errors)
                  <span class="help-block">
                    <strong>{{ $errors->first() }}</strong>
                  </span>
                  @endif
                  <label for="password">Password</label>
                </div>
              </div>
              
              <div class="row">
                <div class="input-field col s12">
                  <input  id="another_password" name="password_confirmation" type="password" class="validate">
                  @if ($errors)
                  <span class="help-block">
                    <strong>{{ $errors->first() }}</strong>
                  </span>
                  @endif
                  <label for="another_password">Confirm Password</label>
                </div>
              </div>

              <div class="row">
                <div class="input-field col s12">
                  <input  id="user-id" name="user_id" type="hidden" class="validate" value="{{ $check->user_id }}">
                </div>
                <div class="input-field col s12">
                  <input  id="verify-code" name="verify_code" type="hidden" class="validate" value="{{ $check->user_code }}">
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                  <button type="submit" class="btn btn-primary">
                    <i class="fa fa-btn fa-sign-in"></i> Submit
                  </button>
                </div>
              </div>
            </form>

            @if(Session::get("errors"))
            <div class="danger text-center">
              {{$errors->first()}}
            </div>
            @endif

          </div>
      </div>
      <div class="col s12 m3"></div>
    </div>
  <div class="col s12">
    

    
   
  </div>
</div>
</div>
</div>
@endsection