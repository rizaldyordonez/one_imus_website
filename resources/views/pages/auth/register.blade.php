@extends('layouts.main')
@section('customCSS')
<style type="text/css">

  .login_padding {
    padding: 100px;
  }

  .panel-heading {
    font-weight: bold;
    font-size: 20px;
  }

  strong{
    color: red;
  }

  .register_bg {
    background: url({{ asset('images/pages/login_bg_02.jpeg') }}) no-repeat center center fixed;
    background-size: cover;
    background-position-y: 60%;
    height: 43.9rem;
  }

  div.g-recaptcha {
    margin: 0 auto;
    width: 304px;
  }
     
</style>
@endsection

@section('content')

<div class="parallax-container register_bg">
<div class="container">
  <div class="row">
    <div class="">
      <div class="col s12 m12">
        <div class="row">
          <div class="col s3"></div>
          <div class="col s12 m6">
            <div class="card hoverable center-align">
              <div class="card-content white-text">
                <h2 class="card-title card-title-black">Please register</h2>

                <form id="login-form" role="form" method="POST" action="{{ route('userRegister') }}">
                   @csrf
                  <div class="row">
                <div class="input-field col s12 m6">
                  <input  id="fullName" name="name" type="text" class="validate" required>
                 @if ($errors->has('name'))
                  <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                  </span>
                  @endif
                  <label for="fullName">Full Name</label>
                  <span class="helper-text"{{--  data-error="wrong" data-success="right" --}}>Name please...</span>
                </div>
                <div class="input-field col s12 m6">
                  <input  id="e-mail" name="email" type="email" class="validate" required>
                  @if ($errors->has('email'))
                  <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                  </span>
                  @endif
                  <label for="e-mail">E-mail</label>
                  <span class="helper-text" {{-- data-error="wrong" data-success="right" --}}>E-mail address only</span>
                </div>
              </div>
              <div class="row">
                
              </div>
              <div class="row">
                <div class="input-field col s12">
                  <input  id="password" name="password" type="password" class="validate" required>
                  @if ($errors->has('password'))
                  <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                  </span>
                  @endif
                  <label for="password">Password</label>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                  <input  id="confirm_password" name="password_confirmation" type="password" class="validate" required>
                  @if ($errors->has('password_confirmation'))
                  <span class="help-block">
                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                  </span>
                  @endif
                  <label for="confirm_password">Confirm Password</label>
                </div>
              </div>

              <div class="row">
                {{-- <div class="col s12 m6 m12">
                    <div class="g-recaptcha" data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY') }}"></div>
               </div> --}}
               <div class="input-field col s12">
                <button type="submit" class="btn btn-primary">
                  <i class="fa fa-btn fa-sign-in"></i> Register
                </button>
              </div>
              </div>
            </form>

          </div>
        </div>
          </div>
          <div class="col s3"></div>
        </div>
      
     </div>
        {{-- <div class="card-action">
          <a href="#">This is a link</a>
          <a href="#">This is a link</a>
        </div> --}}
      
    </div>

</div>
</div>
</div>
@endsection