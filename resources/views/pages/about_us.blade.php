<section class="parallax-container about_us_bg section scrollspy" id="about_us">
	<div class="container">
		<div class="col s12 m12">
			<div class="row">
			<div class="col s12 m12 l12">
				<div class="section">
					<h4>About Us</h4>
				</div>
				<div class="divider"></div>
				<div class="clear_both"></div>
			</div>
		</div>
		</div>

		<div class="col s12 m12">
			<div class="row">
				<div class="col s12 m7">
					<div class="card">
						<div class="card-content" style="max-height: 300px;height:300px;">
							<h6>Company Profile</h6>
							<br/>
							@if(isset($company_profile))
							{!! $company_profile->profile !!}
							@else
							<div>
								<p>
								
								</p>
							</div>
							@endif
						</div>
					</div>
				</div>
				<div class="col s12 m5">
					<div class="card">
						<div class="card-content" style="max-height: 300px;height:300px;">
							<h6>We offer these services :</h6>
							<ol>
								@if(isset($services))
									@foreach ($services as $value)
									<li>{{ $value->service_name }}</li>
									@endforeach
								@endif
							</ol>
						</div>
					</div>
				</div>
			</div>
		</div>
	
		<div class="col s12 m12 l12">
			<div class="row">
				<div class="" style="clear:both; height:10px"></div>
				<div class="col s12 m12">
					<div class="card">
						<div class="card-tabs">
							<ul class="tabs tabs-fixed-width ">
								@if(isset($mission_vision))
								@foreach ($mission_vision as $value)
									<li class="tab"><a @if(strtolower($value->name)=='mission')class="active"@else class="" @endif href="#{{ strtolower($value->name) }}">{{ $value->name }}</a></li>
								@endforeach
								@endif
							</ul>
						</div>
						<div class="card-content grey lighten-4">
							@if(isset($mission_vision))
							@foreach ($mission_vision as $value)
								<div id="{{ $value->name }}" class="center"><p><h6>{{ $value->description }}</h6></p></div>
							@endforeach
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="" style="margin-bottom:20px"></div>
	</div>
	<div class="parallax">
		<img src="{{ asset('images/HD/blank_center.jpg') }}">
	</div>
</section>