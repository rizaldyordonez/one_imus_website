@extends('layouts.main')
@section('content')

<section class="container">
	<div class="col s12 m12">
		<div class="row">
			<div class="col s6 m6">
				<div class="left"><h2> ADD CV/RESUME </h2></div>
				{{-- <div class="clear_both_1"></div>
				<div>{{ strtoupper(Auth::user()->name) }}</div>
				<div class="clear_both_1"></div>
				<div>{{ Auth::user()->email }}</div>
				<div class="clear_both_1"></div> --}}
				
			</div>
			
		</div>
	</div>
	<div class="col s12 m12">
		<div class="row">
			<div class="col s12">
				<ul class="tabs">
					<li class="tab col s3"><a href="#test1">Test 1</a></li>
					<li class="tab col s3"><a class="active" href="#test2">Test 2</a></li>
					<li class="tab col s3 disabled"><a href="#test3">Disabled Tab</a></li>
					<li class="tab col s3"><a href="#test4">Test 4</a></li>
				</ul>
			</div>
			<div id="test1" class="col s12">Test 1</div>
			<div id="test2" class="col s12">Test 2</div>
			<div id="test3" class="col s12">Test 3</div>
			<div id="test4" class="col s12">Test 4</div>
		</div>
	</div>
</section>

@endsection

@section('customCSS')
<style type="text/css">
	.table-body{
		overflow-x: scroll;
		height: 312px;
	}
	.form-modal{
		min-width: 40%;
	}
	.message{
		float: left;
	}
</style>
@endsection

@section('customJS')
<script type="text/javascript">
	
</script>
@endsection