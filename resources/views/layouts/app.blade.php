<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ asset('assets/img/favicon.ico.png') }}" rel="shortcut icon">
    <title>{{$title}}</title>

    <link rel="stylesheet" type="text/css" href="{{asset('css/materialize.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/wow_animate.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Abril+Fatface|Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    @yield('customCSS')
</head>

<main>
    <body>

        @include('layouts.top-nav')
        <div class="">

            @yield('content')

        </div>


    </body>
</main>


<footer class="page-footer orange darken-1">
  <div class="container">
    <div class="row">
      <div class="col l6 s12">
        <h5 class="white-text">Our partners:</h5>
        <ul>
          <li><a class="grey-text text-lighten-3" href="#!">Link 1</a></li>
          <li><a class="grey-text text-lighten-3" href="#!">Link 2</a></li>
          <li><a class="grey-text text-lighten-3" href="#!">Link 3</a></li>
          <li><a class="grey-text text-lighten-3" href="#!">Link 4</a></li>
      </ul>
    </div>
    <div class="col l4 offset-l2 s12">
        <h5 class="white-text">Links</h5>
        <ul>
          <li><a class="grey-text text-lighten-3" href="#!">Link 1</a></li>
          <li><a class="grey-text text-lighten-3" href="#!">Link 2</a></li>
          <li><a class="grey-text text-lighten-3" href="#!">Link 3</a></li>
          <li><a class="grey-text text-lighten-3" href="#!">Link 4</a></li>
      </ul>
  </div>
</div>
</div>
<div class="footer-copyright">
    <div class="container">
        © 2018 ONEIMUS
        <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
    </div>
</div>
</footer>
    
</head>
<body id="app-layout">
    @include('layouts.top-nav')


    @yield('content')


    <footer></footer>
</body>
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous">
</script>
<script type="text/javascript" src="{{asset('js/materialize.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/wow.min.js')}}"></script>

<script type="text/javascript" language="javascript">

 $(document).ready(function(){
  new WOW().init();

  $('.carousel').carousel(
  {
    dist: 0,
    padding: 0,
    fullWidth: true,
    indicators: true,
    height: 1000
  });
 

  $('.modal').modal(); 
  $('.parallax').parallax();
  $('.scrollspy').scrollSpy();
  
  setTimeout(autoplay, 6000);
    //  $(window).load(function(){
    //     $('#preloader').fadeOut('slow',function(){$(this).remove();});
    // });
});

  
function autoplay() {
    $('.carousel').carousel('next');
    setTimeout(autoplay, 6000);
}

function preloader(){
    $('#preloader').show(5);

}
</script>
@yield('customJS')
</html>