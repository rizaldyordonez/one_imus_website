<footer class="page-footer orange darken-1">
  <div class="container">
    <div class="row">
      <div class="col l6 s12">
        <h5 class="white-text">Our partners:</h5>
        <ul>
          <li><a class="grey-text text-lighten-3" href="https://www.sss.gov.ph/" target="_blank">Social Security System</a></li>
          <li><a class="grey-text text-lighten-3" href="https://www.philhealth.gov.ph/" target="_blank">Philhealth</a></li>
          <li><a class="grey-text text-lighten-3" href="https://www.dole.gov.ph/" target="_blank">Department of Labor and Employment</a></li>
          <li><a class="grey-text text-lighten-3" href="https://www.pagibigfundservices.com/" target="_blank">Pag-IBIG</a></li>
          <li><a class="grey-text text-lighten-3" href="https://www.bir.gov.ph/" target="_blank">Bureau of Internal Revenue</a></li>
      </ul>
    </div>
    {{-- <div class="col l4 offset-l2 s12">
      <h5 class="white-text">Affiliated:</h5>
      <ul>
        <li><a class="grey-text text-lighten-3" href="#!">Fortune Life</a></li>
        <li><a class="grey-text text-lighten-3" href="#!">Malayan Insurance</a></li>
        <li><a class="grey-text text-lighten-3" href="#!">Integrated Management Services Inc.</a></li>
      </ul>
    </div>
    <div class="col l4 offset-l2 s12">
      <h5 class="white-text">Member:</h5>
      <ul>
        <li><a class="grey-text text-lighten-3" href="#!">PALSCON (Partnership for Progress and Employment Generation</a></li>
      </ul>
    </div> --}}
</div>
</div>
<div class="footer-copyright">
    <div class="container">
      @php
        $year = date("Y", time());
      @endphp
        © {{ $year }} ONEIMUS
        <a class="grey-text text-lighten-4 right" href="#!"></a>
    </div>
</div>
</footer>





{{-- <script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous">
</script> --}}
<script type="text/javascript" src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/materialize.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/wow.min.js')}}"></script>
<script type="text/javascript" language="javascript">

  function sideNav()
  {
    var instance = M.Sidenav.getInstance('#slide-out');
    instance.open();
  }
  

 $(document).ready(function(){
  new WOW().init();

  $('.carousel').carousel(
  {
    dist: 0,
    padding: 0,
    fullWidth: true,
    indicators: true,
    height: 1000
  });

 
  $('.modal').modal();
  $('.modal-trigger').modal({
    dismissable: false
  }); 
  $('.parallax').parallax();
  $('.scrollspy').scrollSpy();
  $('.sidenav').sidenav();
  $('.tooltipped').tooltip();
 
  $('select').formSelect();
  $('ul.tabs').tabs();
  $('.collapsible').collapsible();

  $("#toContactInfo").click(function() {
    $('ul.tabs').tabs('select', 'contactInfo');
  });

  $("#toWorkInfo").click(function() {
    $('ul.tabs').tabs('select', 'workInfo');
  });

  $("#toEducationInfo").click(function() {
    $('ul.tabs').tabs('select', 'educationInfo');
  });

  $("#toCertificationInfo").click(function() {
    $('ul.tabs').tabs('select', 'certificationInfo');
  });

   $("#toAchievementInfo").click(function() {
    $('ul.tabs').tabs('select', 'achievementInfo');
  });

   $("#toSkillsInfo").click(function() {
    $('ul.tabs').tabs('select', 'skillsInfo');
  });

   $("#toContactRefInfo").click(function() {
    $('ul.tabs').tabs('select', 'referencesInfo');
  });

   $("#toUpload").click(function() {
    $('ul.tabs').tabs('select', 'uploadPhoto');
  });

   
  setTimeout(autoplay, 6000);
  
  $(".navbar-fixed li.spy").click(function(e) {
      //e.preventDefault();
      $('.navbar-fixed li').removeClass('active');
      $(this).addClass('active');
  });

  $(".navbar-fixed #slide-out > li").click(function(e) {
      //e.preventDefault();
      $('.navbar-fixed #slide-out > li.spy').removeClass('active');
      $(this).addClass('active');
  });

  
autoWidth();
});

function autoWidth(){
  var calcRightPos = function(el) { return $tabs_width - el.position().left - el[0].getBoundingClientRect().width - $this.scrollLeft(); };
}

function autoplay() {
    $('.carousel').carousel('next');
    setTimeout(autoplay, 6000);
}

function preloader(){
    $('#preloader').show(5);
}

function checkPrevJob(){

  var value = $('#previous-job').prop('checked');
  if (value===true) {
    $('#previousJob').show();
    $('#newJob').hide();
  }else{
     $('#previousJob').hide();
    $('#newJob').show();
  }
}




</script>

