<div class="navbar-fixed" id="top-nav-next">
	<nav class="orange darken-1" role="navigation">
		<div class="nav-wrapper container topnav">
			<a href="{{ route('showIndex') }}" class="brand-logo">{{$title}}</a>
			<ul id="waves-effect waves-light sidenav" class="right hide-on-med-and-down">
				<div class="left login_distance">
					<li class="spy "><a href="{{ route('showIndex') }}">Home</a></li>
					<li class="spy "><a href="{{ route('showIndex') }}/#about_us">About Us</a></li>
					<li class="spy "><a href="{{ route('showIndex') }}/#contact_us">Contact Us</a></li>
				</div>
				<div class="left">
					<li class="spy @if($route == "showProfile") active @elseif($route == "login") active @endif">
						@if(Auth::check())
						<a href="{{route('showProfile')}}">
							Welcome, {{ strtoupper(Auth::user()->name)}}
						</a>
						@else
						<a href="{{url('login')}}">Login</a>
						@endif
					</li>
					<li class="spy @if($route == "logout") active @elseif($route == "register") active @endif">
						@if(Auth::check())
						<a href="{{url('logout')}}">Logout</a>
						@else
						<a href="{{url('register')}}">Register</a>
						@endif
					</li>
				</div>
				
			</ul>
		</div>
	</nav>
	<ul class="sidenav waves-effect waves-light" id="mobile-demo">
		<li><a href="sass.html">Sass</a></li>
		<li><a href="badges.html">Components</a></li>
		<li><a href="collapsible.html">Javascript</a></li>
		<li><a href="mobile.html">Mobile</a></li>
	</ul>
</div>