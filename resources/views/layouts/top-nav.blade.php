<div class="navbar-fixed">
	<nav class="orange darken-1 top-nav" role="navigation">
		<div class="nav-wrapper container hide-on-med-and-down">
			<a href="{{ route('showIndex') }}" class="brand-logo"><div class="logo-div"><img class="logo-image" src="{{ url($logo) }}"/></div></a>
			<ul id="waves-effect waves-light nav-mobile" class="right hide-on-med-and-down">
				<div class="left login_distance">
					<li class="spy active" 
						@if($route !== "showIndex") style="background-color: rgba(0,0,0,0)!important;"@endif>
						<a  @if($route == "showIndex") href="#home" @else href="{{ route('showIndex') }} @endif" >Home </a>
					</li>
					<li class="spy @if($route !== 'showIndex')d-none @endif">
						<a @if($route == "showIndex") href="#available_jobs" @else href="{{ route('showIndex') }}/#about_us" class="active" @endif>
						Jobs
						</a>
					</li>
					<li class="spy @if($route !== 'showIndex')d-none @endif">
						<a @if($route == "showIndex") href="#about_us" @else href="{{ route('showIndex') }}/#about_us" class="active" @endif>About Us</a>
					</li>
					<li class="spy @if($route !== 'showIndex')d-none @endif">
						<a @if($route == "showIndex") href="#contact_us" @else href="{{ route('showIndex') }}/#contact_us" @endif class="active">Contact Us
						</a>
					</li>
				</div>
				<div class="left">
					<li class="spy @if($route == "login") active @elseif($route == "showProfile") active @endif">
						@if(Auth::check())
						<a href="{{route('showProfile')}}">
							Welcome, {{ strtoupper(Auth::user()->name)}}
						</a>
						@else
						<a href="{{url('login')}}">Login</a>
						@endif
					</li>
					<li class="spy @if($route == "register") active @endif">
						@if(Auth::check())
						<a href="{{url('logout')}}">Logout</a>
						@else
						<a href="{{url('register')}}">Register</a>
						@endif
					</li>
				</div>
			</ul>
		</div>

		<div class="show-on-small sidenav-close" id="mobile">
			<ul id="slide-out" class="sidenav">
				<li class="spy">
					@if(Auth::check())
					<a href="{{route('showProfile')}}" class="@if($route == "login") active @elseif($route == "showProfile") active @endif">
						Welcome, {{ strtoupper(Auth::user()->name)}}
					</a>
					@endif
				</li>
				<li class="spy active " @if($route !== "showIndex") style="background-color: rgba(0,0,0,0)!important;"@endif>
					<a  class=" mobile-nav-color-text" @if($route == "showIndex") href="#home" @else href="{{ route('showIndex') }} @endif">Home </a>
				<li class=" @if($route !== 'showIndex')d-none @endif">
					<a class=" mobile-nav-color-text" @if($route == "showIndex") href="#available_jobs" @else href="{{ route('showIndex') }}/#about_us" class="active" @endif>Jobs</a>
				</li>
				<li class="spy @if($route !== 'showIndex')d-none @endif">
					<a class=" mobile-nav-color-text" @if($route == "showIndex") href="#about_us" @else href="{{ route('showIndex') }}/#about_us" class="active" @endif>About Us</a>
				</li>
				<li class="spy @if($route !== 'showIndex')d-none @endif">
					<a class=" mobile-nav-color-text" @if($route == "showIndex") href="#contact_us" @else href="{{ route('showIndex') }}/#contact_us" @endif class="active">Contact Us
					</a>
				</li>
				<li class="spy">
					@if(!Auth::check())
					<a href="{{url('login')}}" class="@if($route == "login") active @elseif($route == "showProfile") active @endif">Login</a>
					@endif
				</li>
				<li class="spy">
					@if(Auth::check())
					<a href="{{url('logout')}}" class="@if($route == "register") active @elseif($route == "showProfile") @endif">Logout</a>
					@else
					<a href="{{url('register')}}" class="@if($route == "register") active @elseif($route == "showProfile") active @endif">Register</a>
					@endif
				</li>
			</ul>

			
			<a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons">menu</i></a>
			{{-- <a href="{{ route('showIndex') }}" class="brand-logo"><div class="logo-div"><img class="logo-image hide-on-med-and-down" src="{{ url($logo) }}"/></div></a> --}}
		</div>
	</nav>
</div>

@section('customJS')
<script type="text/javascript">
</script>

@endsection