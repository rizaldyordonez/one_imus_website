<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ asset('assets/img/favicon.ico.png') }}" rel="shortcut icon">
    <title>{{$title}}</title>

    <link rel="stylesheet" type="text/css" href="{{asset('css/materialize.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/wow_animate.css')}}">
    {{-- <link href="https://fonts.googleapis.com/css?family=Abril+Fatface|Roboto" rel="stylesheet"> --}}
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link rel="stylesheet" type="text/css" href="{{asset('css/hover.css')}}" >
    
    <style type="text/css">
    body {
      display: flex;
      min-height: 100vh;
      flex-direction: column;
    }

    main {
      flex: 1 0 auto;
    }

  /*Home*/
  .carousel.carousel-slider .carousel-item h2 {
    font-size: 30px;
    font-weight: 500;
    line-height: 100px;
    background-color: #fb8c0070;
    width: 700px;
  }

  .parallax-container {
    height: auto;
  }

  .company_name{
    color: #074a69;
    text-align: left;
    font-family: 'Roboto', sans-serif;
    font-family: 'Abril Fatface', cursive;
    margin-top: 260px;
  }

  .sub_name{
    color: #074a69;
    text-align: left;
    font-family: 'Roboto', sans-serif;
    font-family: 'Abril Fatface', cursive;
  }

  .carousel{
    height:70vh !important;
  }

  #slider h2, p {
    color: #000000;
    font-family: 'Roboto', sans-serif;
    /*font-family: 'Abril Fatface', cursive;*/
  }

  .card_space {
    margin: 200px 0px;
  }
  .card-title-black { 
    color: #000;
  }
  .divider {
    height: 1px;
    overflow: hidden;
    background-color: #aba7a7;
  }
  .about_us_bg {
    background: rgba(0, 62, 189, 0.73);
  }
  #about_us h4, #available_jobs h4 {
    color: #ffffff;
  }

  .circle {
    margin: 10px 5px;
    width: 100px;
    color: #fff;
    font-size: 12px;
    line-height: 100px;
    text-align: center;
    height: 100px;
    border-radius: 100px;
  }

  .dim_bg {
    background-color: #28252578;
  }

  #about_us blockquote {
    border-left-color: #ffe0b2;
  }

  #about_us .tabs .indicator {
    background-color: #ffe0b2;
  }

  #about_us .tabs .tab a{
    color: #fb8c00;
  }

  #about_us .tabs .tab a:hover, .tabs .tab a.active {
    background-color: transparent;
    color: #fb8c00;
  }

  .section_custom {
    padding-top: 12rem;
    padding-bottom: 1rem;
  }

  .card-image-max{
    max-height: 274px;
  }
  /*End of Home*/

  /*Insert / Add Resume*/
  .resume-add-experience {
    float: right;
    margin-bottom: 10px;
  }


  /*Profile*/
  .profile-btn-right{
    padding: 0px 4px;
  }

  .modal-ask {
    width: 30%!important;
    height:20%!important;
  }

  .select-dropdown{
    overflow-y: auto !important;
  }

  .dropdown-content {
    max-height: 250px;
    backface-visibility: hidden !important;
  }
  /*End of Profile*/
    .clear_both {
      clear: both;
      height: 4rem;
    }

    .clear_both_1 {
      clear: both;
      height: 1rem;
    }

    .login_distance {
     margin-right: 30px;
   }

   /*input[type="text"] + label { pointer-events: all!important; }*/

   .input-field label { pointer-events: all!important; }

   .d-none {
    display: none;
  }
  .sentence{
    text-transform: capitalize;
  }
  .uppercase{
    text-transform: uppercase;
  }

  .success-req {
    margin-top: 35px;
    margin-left: 125px;
    color: #2fce2f;
  }

  .show-see-more {
    text-align: center;
  }

  .see-more:hover{
    color: #fff;
    text-decoration: underline;
  }

  .see-more {
   text-decoration: underline;
  }

  .see-full-list {
   text-decoration: underline;
   color: #fff;
  }

  .see-full-details {
    color: #fff;
    text-decoration: underline;
  }

  .hvr-bounce-to-bottom {
    width: 100%;
    display: inline-block;
    vertical-align: middle;
    -webkit-transform: perspective(1px) translateZ(0);
    transform: perspective(1px) translateZ(0);
    box-shadow: 0 0 1px rgba(0, 0, 0, 0);
    position: relative;
    -webkit-transition-property: color;
    transition-property: color;
    -webkit-transition-duration: 0.5s;
    transition-duration: 0.5s;
  }

  .hvr-bounce-to-bottom:before {
    content: "";
    position: absolute;
    z-index: -1;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background: #fb8c00;
    -webkit-transform: scaleY(0);
    transform: scaleY(0);
    -webkit-transform-origin: 50% 0;
    transform-origin: 50% 0;
    -webkit-transition-property: transform;
    transition-property: transform;
    -webkit-transition-duration: 0.5s;
    transition-duration: 0.5s;
    -webkit-transition-timing-function: ease-out;
    transition-timing-function: ease-out;
  }

  .see-full-categories {
    position: relative;
    z-index: 2;
    -webkit-filter: blur(8px);
    width: 100%;
    filter: 8px;
    left: 0;
    right:0;
    height: 75px;
  }

  .overlay-disabled {
    pointer-events: none;
    display: none;
  }

  /*CMS*/
  .logo-image {
    max-height: inherit;
    max-width: inherit;
  }

  .logo-div {
    max-height: 63.5px;
    max-width: 145px;
    overflow: hidden;
  }

  #sidenav-overlay { z-index: 1; }
  .navbar-fixed { z-index: 998; }
  /*END CMS*/

  /*Mobile*/
  .mobile-nav-color-text{
    color: #ffffff!important;
  }
  .show-on-small ul>li>a {
    color: #fb8c00 !important;
  }
  .show-on-small ul>li>a.active {
    background-color: #fb8c00;
    color: #ffffff !important;
  }
    </style>
    
    @yield('customCSS')

</head>
<body>

 
    <main>
    
    {!! ""; $route = \Route::currentRouteName(); !!}
    {!! ""; $url = url()->current() !!}

    @include('layouts.top-nav')

    @yield('content')

    </main>
</body>

@include('layouts.footer')
@yield('customJS')

</html>