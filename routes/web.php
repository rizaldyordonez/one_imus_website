<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
// Auth::routes();
Route::get('/',["uses"=>"HomeController@showIndex", "as"=>"showIndex"]);
Route::get('/about-us',["uses"=>"HomeController@showAbout", "as"=>"showAbout"]);
Route::get('/contact-us',["uses"=>"HomeController@showContact", "as"=>"showContact"]);
Route::get('/jobs/{job_category}',["uses"=>"HomeController@showJobs", "as"=> "showJobs"]);
Route::get('/jobs/{job_category}/{job_category_list}',["uses"=>"HomeController@showJobs", "as"=> "showJobsInfo"]);
Route::get('/all-jobs/{bool}',["uses"=>"HomeController@showAllJobs", "as"=> "showAllJobs"]);

Route::get('login',["uses"=>"Auth\LoginController@index", "as"=>"login"]); //loginPage
Route::get('register',["uses"=>"Auth\RegisterController@showRegistrationForm", "as"=>"register"]);
Route::get('forgot-password',["uses"=>"ForgotPasswordsController@showForgotPassword", "as"=> "showForgotPassword"]);
Route::get('forgot-password-verification',["uses"=>"ForgotPasswordsController@showCodeVerification", "as"=> "showCodeVerification"]);
Route::get('change-password/{verify_code}',["uses"=>"ForgotPasswordsController@showChangePassword", "as"=> "showChangePassword"]);

//Detecting directory path when upload to hosting service
Route::get('/getPath',["uses"=>"HomeController@getPath", "as"=> "getPath"]);

Route::post('do-login',["uses"=>"Auth\LoginController@login", "as"=>"userLogin"]);
Route::post('do-register',["uses"=>"Auth\RegisterController@register", "as"=>"userRegister"]);
Route::post('do-submit-verification',["uses"=>"ForgotPasswordsController@doSubmitVerification", "as"=>"doSubmitVerification"]);
Route::any('itexmo',["uses"=>"ForgotPasswordsController@itexmo", "as"=>"itexmo"]);
Route::post('do-verify-code',["uses"=>"ForgotPasswordsController@doConfirmVerification", "as"=>"doConfirmVerification"]);
Route::post('do-change-password',["uses"=>"ForgotPasswordsController@doChangePassword", "as"=>"doChangePassword"]);


Route::post('/send-message',["uses"=>"ContactController@index", "as"=>"sendMessage"]);

Route::post('/send-email',["uses"=>"MailController@contactus_email", "as"=>"sendMail"]);


Route::group(['middleware' => ['auth']], function() {
	Route::get('logout', 'Auth\LoginController@logout')->name('logout');
	Route::get('/your-profile',["uses"=>"ProfileController@showProfile", "as"=>"showProfile"]);
	Route::get('/your-profile/showResume',["uses"=>"ProfileController@showPDF", "as"=>"showPDF"]);
	Route::get('/your-profile/testPDF',["uses"=>"ProfileController@testPDF", "as"=>"testPDF"]);

	Route::get('/insert-your-resume',["uses"=>"ProfileController@showInsertResume", "as"=>"showInsertResume"]);
	Route::get('/insert-your-pdf',["uses"=>"ProfileController@showInsertPDF", "as"=>"showInsertPDF"]);

	Route::get('/insert-your-resume/showAddWork',["uses"=>"ProfileController@showAddWork", "as"=>"showAddWork"]);
	Route::get('/upload-your-requirements',["uses"=>"ProfileController@showUploadRequirements", "as"=>"showUploadRequirements"]);

	Route::any('/do-insert-resume',["uses"=>"ProfileController@doInsertResume", "as"=>"doInsertResume"]);
	Route::any('/do-submit-resume',["uses"=>"ProfileController@doSubmitResume", "as"=>"doSubmitResume"]);
	Route::any('/do-upload-requirements',["uses"=>"ProfileController@doUploadRequirements", "as"=>"doUploadRequirements"]);

	//email verification
	Route::get('/reverify-email',["uses"=>"ProfileController@resendEmailVerification", "as"=>"reverifyMail"]);
	Route::get('/verify-email',["uses"=>"ProfileController@verifyEmail", "as"=>"verifyMail"]);

	//change email
	Route::post('/change-email',["uses"=>"ProfileController@changeEmailVerification", "as"=>"changeMail"]);

	//Change Password
	Route::post('/change-pass',["uses"=>"ProfileController@changePassword", "as"=>"changePassword"]);
});