<?php

namespace App\Repositories;

use Request;
use App\Models\User;
use App\Models\UserLog;
use App\Models\UserResumeModel;
use App\Models\UserPdf;
use App\Models\Application;
use App\Models\ForgotPassword;
use App\Models\UserContentJob;
use App\Models\Verification;
use App\Models\Work;

/**
* Bank resource repository
*/
class UserRepository
{
	private $user;
	private $userLog;

	function __construct( 
		User $user, 
		UserLog $userLog, 
		UserResumeModel $userResume, 
		UserPdf $userpdf, 
		Application $adminApplication, 
		ForgotPassword $forgotPassword, 
		UserContentJob $userContentJob,
		Verification $verification,
		Work $work

	){

		$this->user = $user;
		$this->userLog = $userLog;
		$this->userResume = $userResume;
		$this->userpdf = $userpdf;
		$this->adminApplication = $adminApplication;
		$this->forgotPassword = $forgotPassword;
		$this->userContentJob = $userContentJob;
		$this->verification = $verification;
		$this->work = $work;

	}

	function getJobLists()
	{
		$jobs = $this->userContentJob->select("name")->where("status",1)
							 ->groupBy("name")
							 ->get();
		return $jobs;
	}

	function insertNew($request)
	{
		$this->user->name = $request->name;
		$this->user->email = $request->email;
		$this->user->password = bcrypt($request->password);
		$this->user->type = 0;
		$this->user->verify = 0;
		$this->user->status = 1;
		
		$this->user->save();
		
		return $this->user;
	}

	function updateEmailByUserId($user_id, $email)
	{
		$user = $this->user->where("id","=",$user_id)->first();

		if ($user) {
			$user->email = $email;
			$user->save();
			return true;
		}
		
		return false;
	}

	function verifyUserById($user_id)
	{
		$user = $this->user->where("id","=",$user_id)->first();
		if ($user) {
			$user->verify = 1;
			$user->save();

			return true;
		}

		return false;
	}

	function insertNewResume($request,$userid)
	{
		$model = $this->userResume->where("resume_id","=",$request->id)->first();

		if(!$model){
			$model = new $this->userResume;
		}

		if($request->hasFile('resume_photo')) {
            $path = file_get_contents($request->file('resume_photo')->path());
            $destinationPath_website = "../public/images/picture";
            if(!is_dir($destinationPath_website)){
                $dir = mkdir($destinationPath_website, 0777, true);
            }

            $image = file_put_contents($destinationPath_website.'/'.$userid.'-picture.jpg', $path);

            $destinationPath_admin = "../../one_imus/public/images/picture";
            if(!is_dir($destinationPath_admin)){
                $dir = mkdir($destinationPath_admin, 0777, true);
            }

            $image = file_put_contents($destinationPath_admin.'/'.$userid.'-picture.jpg', $path);
        }

        if($request->file('cert')) {

            $files = $request->file('cert');

            $destinationPath_website = "../public/images/certificates";
            if(!is_dir($destinationPath_website)){
                $dir = mkdir($destinationPath_website, 0777, true);
            }

            $destinationPath_admin = "../../one_imus/public/images/certificates";
            if(!is_dir($destinationPath_admin)){
                $dir = mkdir($destinationPath_admin, 0777, true);
            }
            
            $count = 1;
            $certificates_data = [];
            foreach($files as $file){
                $path = file_get_contents($file->path());
                $image = file_put_contents($destinationPath_website.'/'.$userid.'-certificate-'.$count.'.jpg', $path);
                $image = file_put_contents($destinationPath_admin.'/'.$userid.'-certificate-'.$count.'.jpg', $path);
                $certificates_data[] = $userid.'-certificate-'.$count.'.jpg';
                $count++;
                
            }
        }

        $model->user_id = $userid;
        $model->resume_picture = $userid.'-picture.jpg';
		$model->resume_firstname = ucwords($request->firstname);
		$model->resume_middlename = ucwords($request->middlename);
		$model->resume_lastname = ucwords($request->lastname);
		$model->resume_age = $request->resume_age;
		$model->resume_birthdate = $request->birthdate;
		$model->resume_nationality = ucwords($request->nationality);
		$model->resume_civil_status = ucwords($request->civil_status);
		$model->resume_gender = ucwords($request->gender);
		$model->resume_email = $request->email;
		$model->resume_homeaddress = ucwords($request->homeaddress);
		$model->resume_city = ucwords($request->city);
		$model->resume_mobilenumber = $request->mobilenumber;

		$model->resume_father = ucwords($request->resume_father);
		$model->resume_mother = ucwords($request->resume_mother);
		$model->resume_father_contact = $request->resume_father_contact;
		$model->resume_mother_contact = $request->resume_mother_contact;

		$model->resume_region = ucwords($request->region);
		$model->resume_objective = ucwords($request->work_objective);

		$model->resume_newjob = ucwords($request->choose_available_jobs);
		$model->resume_prevjobs = (!is_null($request->prevDetails[0][0]))? json_encode($request->prevDetails) : null;
		$model->resume_achievement = (!is_null($request->achievement[0][0]))? json_encode($request->achievement) : null;

		$model->resume_edu_attain = ucwords($request->educationalAttainment);
		$model->resume_school = ucwords($request->schoolname);
		$model->resume_field_study = ucwords($request->fieldOfStudy);


		$model->resume_gradate = $request->graduateDateFrom;
		$model->resume_gradate_to = $request->graduateDateTo;

		$model->resume_certification = (isset($certificates_data)) ? json_encode($certificates_data) : null;
		$model->resume_cert_year_taken = $request->cert_year_taken;
		$model->resume_skills = json_encode($request->skills);
		$model->resume_references = json_encode($request->ref_data);
		$model->created_at = time();
		$model->save();

		if ($model->save()===true) {

			$data['response'] = true;
			$data['message'] = "CV added successfully";
		}else{

			$data['response'] = false;
			$data['message'] = "CV not added";
		}

		return $data;
	}

	function submitResumeById($user_id,$resume_id,$job_id)
	{

		$model = $this->adminApplication->where("user_id",$user_id)
										->where("resume_id",$resume_id)
										->where("job_id", null)
										->first();
		if (!$model) {
			$model = new $this->adminApplication;
			$model->user_id = $user_id;
			$model->resume_id = $resume_id;
		}

		$model->job_id = (isset($job_id)) ? $job_id : null;
		$model->stage = 0;
		$model->status = 0;
		$model->is_done = 0;
		$model->note = 0;
		$model->created_at = time();
		$model->updated_at = time();

		if ($model->save()) {
			$response['success'] = true;
		}else{
			$response['success'] = false;
		}
		return $response;
	}

	function checkResumeById($user_id)
	{
		$app = $this->adminApplication->where("user_id","=",$user_id)->where("is_done","=",0)->first();

		return $app;
	}

	function submitPDFById($user_id, $pdf_id, $file_name)
	{

		$model = $this->userpdf->where("user_id",$user_id)->where("id",$pdf_id)->first();

		if (!$model) {
			$model = new $this->userpdf;
			$model->user_id = $user_id;
		}

		$model->file_name = $file_name;
		$model->created_at = time();
		$model->updated_at = time();

		if ($model->save()) {
			$response['success'] = true;
		}else{
			$response['success'] = false;
		}
		return $response;
	}

	function checkPDFByUserId($user_id)
	{
		$pdf = $this->userpdf->where("user_id",$user_id)->first();

		return $pdf;
	}

	function checkResumeByEverything($user_id,$partner_id)
	{
		$app = $this->adminApplication->where("user_id","=",$user_id)->where("is_done","=",0)->first();

		return $app;
	}

	function insertNewLog($user_id, $activity, $desc)
	{
		$this->userLog->user_id = $user_id;
		$this->userLog->activity = $activity;
		$this->userLog->activity_desc = $desc;
		$this->userLog->ip_address = \Request::ip();

		$this->userLog->save();
		
		return true;
	}

	function getResume($userid)
	{
		$resume = $this->userResume->where("user_id","=",$userid)->first();
		
		return $resume;
	}

	function getLogs($user_id)
	{

		$log = $this->userLog->where('user_id', $user_id)
							->where("activity","like","Log%")
							->orderby('created_at', 'desc')
							->limit(10)
							->get();
		
		return $log;
	}

	function getResumeLogs($user_id)
	{
		$log = $this->userLog->where("activity","like","%resume%")
							 ->where('user_id', $user_id)
							 ->orderby('created_at', 'desc')
							 ->limit(10)
							 ->get();

		return $log;
	}

	function verifyAccount($user_email,$user_name)
	{
		
		$user = $this->user->select("id","name")
								 ->where("email", $user_email)
								 ->where("name", $user_name)
								 ->first();
		if ($user) {
			$data['response'] = true;
			$data['user_name'] = $user->name;
			$data['user_id'] = $user->id;
		}else{
			$data['response'] = false;
			$data['user_name'] = 'test';
		}

		return $data;
	}

	function insertVerificationCode($verification_code, $user_id, $user_name)
	{
		$this->forgotPassword->user_id = $user_id;
		$this->forgotPassword->user_name = $user_name;
		$this->forgotPassword->user_code = $verification_code;
		$this->forgotPassword->done = 0;
		$this->forgotPassword->save();

		return $this->forgotPassword;
	}

	function verifyCode($verify_code)
	{
		$verify = $this->forgotPassword->where("user_code","=",$verify_code)->first();
		return $verify;
	}

	function update_password($password,$user_id)
	{
		$model = $this->user->find($user_id);
		
		if ($model->password) {
			$model->password = $password;
			$model->id = $model->id;
		}

		if (!$model->save()) {
			return false;
		}else{
			return true;
		}
	}

	function checkUserAndCode($verify_code)
	{
		$check = $this->forgotPassword->where('user_code',$verify_code)->where('done',0)->first();
		return $check;
	}

	function update_code_status($verify_code,$user_id)
	{
		$model = $this->forgotPassword->where('user_id',$user_id)->where('user_code',$verify_code)->first();
		
		if ($model) {
			$model->id = $model->id;
			$model->done = 1;
		}

		if (!$model->save()) {
			return false;
		}else{
			return true;
		}
	}

	/*-----------[START] EMAIL VERIFICATION-------------*/

	function insertEmailVerificationCode($user_id, $code){

		$this->verification->user_id = $user_id;
		$this->verification->code = $code;
		$this->verification->status = 1;
		
		if ($this->verification->save()) {
			return true;
		}

		return false;
	}

	function getEmailVerificationCode($user_id, $code){

		$verification = $this->verification->where("user_id","=",$user_id)->where("code","=",$code)->where("status","=", 1)->first();

		return $verification;
	}

	function updateEmailVerificationCodeById($user_id, $code){

		$verification = $this->verification->where("user_id","=",$user_id)->where("code","=",$code)->where("status","=", 1)->first();

		if ($verification) {
			$verification->status = 0;
			$verification->save();

			return true;
		}

		return false;
	}

	function checkBeforeSubmit($user_id,$job_id,$partner_id){
		//dd($user_id.' '.$job_id.' '.$partner_id);

		//Checks if the user have been deactivated by the partner
		$work = $this->work->select('work.user_id')
						   ->leftJoin('application','application.user_id','=','work.user_id')
						   ->where('work.status','0')
						   ->where('application.is_done','1')
						   ->whereNotIn('application.status',['Failed','Rejected'])
						   ->where('work.user_id',$user_id)
						   ->where('work.partner_id',$partner_id)
						   ->first();

		//Checks if the user already applied to the same job id and was already marked as done
		$existing_job_application = $this->adminApplication->select('user_id')
							  ->where('user_id',$user_id)
							  ->where('is_done',0)
							  ->whereNotIn('status', ['Failed','Rejected'])
							  ->first();

		//Checks if the user have been rejected or failed by the system
		$failed_application = $this->adminApplication->select('user_id')
									     ->where('user_id',$user_id)
									     ->where('job_id',$job_id)
									     ->whereIn('status', ['Failed','Rejected'])
									     ->first();
		
		$data['work'] = $work;
		$data['failed_application'] = $failed_application;
		$data['existing_job_application'] = $existing_job_application;

		return $data;
	}

	/*-----------[END] EMAIL VERIFICATION-------------*/
}