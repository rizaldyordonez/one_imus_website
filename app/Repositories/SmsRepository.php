<?php

namespace App\Repositories;

use Twilio\Rest\Client;
use Twilio\Exceptions\TwilioException;
use Exception;

class SmsRepository
{
    private $twilio_account_sid = 'AC8b998b037237732324be91d6c60e4bc9';
    private $twilio_auth_token = '9c89e06340347e38faab6a36d90c9722';
    private $twilio_phone_number = '+12014742353';
    
    public function twillio($to, $message)
    {

        // $accountSid = env('TWILIO_ACCOUNT_SID');
        // $authToken = env('TWILIO_AUTH_TOKEN');
        // $twilioNumber = env('TWILIO_PHONE_NUMBER');
        $accountSid = $this->twilio_account_sid;
        $authToken = $this->twilio_auth_token;
        $twilioNumber = $this->twilio_phone_number;
        $result = '';
       
        $client = new Client($accountSid, $authToken);


        try {
            $result = $client->messages->create(
                $to,
                [
                    "body" => $message,
                    "from" => $twilioNumber
                ]
            );
        } catch (TwilioException $e) {
            echo  $e;
        }

        return $result;
    }

    public function itexmo($number,$message){

        $apicode = 'TR-ONEIM433370_NC5AC';

        try {

            $ch = curl_init();
            $itexmo = array('1' => $number, '2' => $message, '3' => $apicode);
            curl_setopt($ch, CURLOPT_URL,"https://www.itexmo.com/php_api/api.php");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, 
                http_build_query($itexmo));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $res = curl_exec ($ch);
            

        } catch (Exception $e) {
            $res = $e;
        }

        return $res;
        curl_close ($ch);
    }
}
