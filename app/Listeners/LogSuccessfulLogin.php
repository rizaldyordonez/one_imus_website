<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Login;
use Illuminate\Http\Request;

use App\Repositories\UserRepository;

class LogSuccessfulLogin
{
    private $user;
    /**
     * Create the event listener.
     *
     * @param  Request  $request
     * @return void
     */
    public function __construct(Request $request, UserRepository $user)
    {
        $this->request = $request;
        $this->user = $user;
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        $user = $event->user;
        $user->login_date = time();
        $user->login_ip = $this->request->ip();
        
        if ($user->save()) {
            $this->user->insertNewLog($user->id, 'Login', 'Success');
        }
    }
}