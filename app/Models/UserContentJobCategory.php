<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserContentJobCategory extends Model
{
    protected $primaryKey = 'id';

	protected $table = 'job_category';
	protected $dateFormat = 'U';
	public $timestamp = true;
}
