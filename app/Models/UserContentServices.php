<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserContentServices extends Model
{
    protected $primaryKey = 'service_id';

	protected $table = 'content_services';
	protected $dateFormat = 'U';
	public $timestamp = true;
}
