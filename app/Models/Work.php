<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Work extends Model
{
    protected $primaryKey = 'id';

	protected $table = 'work';
	protected $dateFormat = 'U';
	public $timestamp = true;
	
}
