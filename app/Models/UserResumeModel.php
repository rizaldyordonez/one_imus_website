<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserResumeModel extends Model
{
    protected $primaryKey = 'resume_id';

	protected $table = 'user_resume';
	protected $dateFormat = 'U';
	public $timestamp = true;
}
