<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserContentLogo extends Model
{
    protected $primaryKey = 'id';

	protected $table = 'content_logo';
	protected $dateFormat = 'U';
	public $timestamp = true;
}
