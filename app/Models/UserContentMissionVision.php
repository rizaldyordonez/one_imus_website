<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserContentMissionVision extends Model
{
    protected $primaryKey = 'id';

	protected $table = 'content_mission_vision';
	protected $dateFormat = 'U';
	public $timestamp = true;
}
