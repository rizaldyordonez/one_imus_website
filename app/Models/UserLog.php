<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLog extends Model
{
    protected $primaryKey = 'id';

	protected $table = 'users_log';
	protected $dateFormat = 'U';
	public $timestamp = true;
	
}
