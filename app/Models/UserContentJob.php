<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserContentJob extends Model
{
    protected $primaryKey = 'id';

	protected $table = 'job';
	protected $dateFormat = 'U';
	public $timestamp = true;
}
