<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserContentCompanyProfile extends Model
{
    protected $primaryKey = 'id';

	protected $table = 'content_company_profile';
	protected $dateFormat = 'U';
	public $timestamp = true;
}
