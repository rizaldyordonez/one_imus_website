<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRequirements extends Model
{
    protected $primaryKey = 'id';

	protected $table = 'user_requirements';
	protected $dateFormat = 'Y-m-d H:i:s';
	public $timestamp = true;
}
