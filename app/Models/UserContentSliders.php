<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserContentSliders extends Model
{
    protected $primaryKey = 'slider_id';

	protected $table = 'content_sliders';
	protected $dateFormat = 'U';
	public $timestamp = true;
}
