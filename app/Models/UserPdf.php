<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPdf extends Model
{
    protected $primaryKey = 'id';

	protected $table = 'user_pdf';
	protected $dateFormat = 'U';
	public $timestamp = true;
}
