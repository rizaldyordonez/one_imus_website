<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    protected $primaryKey = 'id';

	protected $table = 'application';
	protected $dateFormat = 'U';
	public $timestamp = true;
	
}
