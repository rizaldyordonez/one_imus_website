<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Verification extends Model
{
    protected $primaryKey = 'id';

	protected $table = 'verification';
	public $timestamp = true;
	
}
