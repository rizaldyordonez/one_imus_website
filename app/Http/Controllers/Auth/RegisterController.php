<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;

use Validator;
use Auth;
use Redirect;

use App\User;
use App\Repositories\UserRepository;
use App\Models\UserContentLogo;

use App\Http\Controllers\MailController;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';
    private $user;
    private $mail;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserRepository $user, MailController $mail)
    {
        $this->middleware('guest');
        $this->user = $user;
        $this->mail = $mail;
    }


    public function showRegistrationForm(Request $request){
        
        $data['title'] = "ONEIMUS";
        $image_name = UserContentLogo::select("logo_image")->first();
        $image_name = $image_name->logo_image;
        $logo = "/images/logo/".$image_name;
        $data['logo'] = $logo;

        return view('pages.auth.register', $data);
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'name'          => 'required|unique:users,name|min:4|regex:/^[a-zÑñA-Z\s]*$/',
                'password'      => 'required|confirmed|min:6',
                'email'         => 'required|email|unique:users,email'
            ]);
        if ($validator->fails()) {

            $errors = $validator->errors();
            return back()
                ->withErrors($errors)
                ->withInput();
        }else{

            $user = User::create([
                        'name' => $request->name,
                        'email' => $request->email,
                        'password' => bcrypt($request->password),
                        'type' => 0,
                        'verify' => 0,
                        'status' => 1,
                    ]);

            if ($user) {

                //insert log
                $log = $this->user->insertNewLog($user->id, 'Register', 'Account '.$request->name.' created.');

                $this->guard()->login($user);

                //generate verification code
                $veri_code = md5(microtime());

                //isert email verification code
                $verification = $this->user->insertEmailVerificationCode($user->id, $veri_code);

                //send email
                if ($verification === true) 
                {
                   $data['title'] = "ONEIMUS";
                   $image_name = UserContentLogo::select("logo_image")->first();
                   $image_name = $image_name->logo_image;
                   $logo = "/images/logo/".$image_name;
                   $data['logo'] = $logo;

                   $email = $this->mail->verification_email($request->name, $veri_code, $request->email);

                   $data['success'] =  $email['success'];
                   $data['message'] =  $email['message'];

                   return view('pages.profile.email-verify', $data);
                }
                else
                {
                    return redirect('/your-profile');
                }
            }
        }
    }
}
