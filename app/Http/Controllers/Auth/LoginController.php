<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use App\Repositories\UserRepository;
use App\Models\UserContentLogo;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';
    private $user;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserRepository $user)
    {
        $this->middleware('guest')->except('logout');
        $this->user = $user;
    }

    public function index()
    {
        $image_name = UserContentLogo::select("logo_image")->first();
        $image_name = $image_name->logo_image;
        $logo = "/images/logo/".$image_name;
        $data['title'] = "ONEIMUS";
        $data['logo'] = $logo;

        $data['login_header'] = 'Please login';
        return view('pages.auth.login', $data);
    }

    public function logout(Request $request) 
    {
        $logout = $this->user->insertNewLog(Auth::user()->id, 'Logout', 'Logout');
        if ($logout === true) {
            Auth::logout();
        }

        return redirect('/');
    }
}
