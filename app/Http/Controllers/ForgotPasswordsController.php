<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Repositories\SmsRepository;
use App\Repositories\UserRepository;
use App\Models\UserContentLogo;
use Illuminate\Support\Str;
use Redirect;
use Session;
use Validator;
use Hash;

class ForgotPasswordsController extends Controller
{

	private $sms;
	private $user;

	public function __construct(SmsRepository $sms, UserRepository $user){
		$this->sms = $sms;
		$this->user = $user;
	}

    public function showForgotPassword(Request $request){

        $data['title'] = "ONEIMUS";
        $image_name = UserContentLogo::select("logo_image")->first();
        $image_name = $image_name->logo_image;
        $logo = "/images/logo/".$image_name;
        $data['logo'] = $logo;

        return view('pages.auth.forgot-password', $data);
    }

    public function showCodeVerification(Request $request){

        $data['title'] = "ONEIMUS";
        $image_name = UserContentLogo::select("logo_image")->first();
        $image_name = $image_name->logo_image;
        $logo = "/images/logo/".$image_name;
        $data['logo'] = $logo;

        return view('pages.auth.forgot-password-verification', $data);
    }

    public function showChangePassword(Request $request){

        $data['title'] = "ONEIMUS";
        $image_name = UserContentLogo::select("logo_image")->first();
        $image_name = $image_name->logo_image;
        $logo = "/images/logo/".$image_name;
        $data['logo'] = $logo;

        $check = $this->user->checkUserAndCode($request->verify_code);

        if ($check) {

            if ($check->done===true) {
                $data['errors'] = 'Sorry, that code was already used.';
                return back()->withErrors($data);
            }else{
               $data['check'] = $check;
               return view('pages.auth.change-password', $data);
            }
        }else{
            $data['errors'] = 'Sorry, the code does not exist.';
           return back()->withErrors($data);
        }
    }

    public function doSubmitVerification(Request $request){

    	$email = $request['email'];
    	$user_name = $request['user_name'];
    	$contact = $request['contact'];

    	$findUser = $this->user->verifyAccount($email, $user_name);
        
    	if ($findUser['response']) {

    		$verification_code = Str::random(6);
            $verification_code = strtoupper($verification_code);
            $user_id = $findUser['user_id'];
            $user_name = $findUser['user_name'];

            $message = "This is your verification code: ".$verification_code;

            if(substr($contact,0,1)=="0")
            {
                $contact="+63".substr($contact,1,strlen($contact));
            }
            elseif(substr($contact,0,1)=="9")
            {
                $contact="+63".$contact;
            }

            $insertVerification = $this->user->insertVerificationCode($verification_code, $user_id, $user_name);
            
            $sms = $this->sms->itexmo($contact, $message);
            //$sms = 'queued';
            
            if ($sms->status != 'queued') {
                return back()->withInput();
            }else{

                return redirect()->route('showCodeVerification');
            }
    	}else{
    		return back()->withErrors(['msg_error'=>'No such user']);
    	}
    }

    public function doConfirmVerification(Request $request){
       
      $verify_code = strtoupper($request->verify_code);
      $verify = $this->user->verifyCode($request->verify_code);
      
      if ($verify) {
            
            $data['verify_code'] = $verify->user_code;
            return redirect()->route('showChangePassword', $data);
      }else{

            return back()->withErrors('Invalid code!');
      }
    }
    

    public function doChangePassword(Request $request){

        $validator = Validator::make($request->all(), [
            'password'  => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6'
        ]);

        if ($validator->fails()) {

            $result['success'] = false;
            $result['message'] = $validator->messages()->first();

            return back()->withErrors($result['message']);
        }else{
   
           $new_password = bcrypt($request->password);
           $update_password = $this->user->update_password($new_password, $request->user_id);

           if ($update_password) {

            $log = $this->user->insertNewLog($request->user_id, 'Change Password', 'Updated new password');
            $update_code_status = $this->user->update_code_status($request->verify_code, $request->user_id);
            
            $result['success'] = true;
            $result['message'] = 'Password updated. Please login again.';
            sleep(2);
            return redirect()->route('login')->with($result);
            }else{
                $data['errors'] = 'Failed to update the password.';
                return back()->withErrors($data);
            }
        }
    }
}
