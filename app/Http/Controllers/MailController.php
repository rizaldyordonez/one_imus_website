<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Mail;
use Validator;
use Config;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Verification;

class MailController extends Controller {

   public function contactus_email(Request $request) {

	   	$rules = [
	   		'email' => 'required|regex:/^[\pL\s\-]+$/u',
	   		'email' => 'required|email',
	   		'message' => 'required'
	   	];

	   	$validator = Validator::make($request->all(), $rules);

	   	if ($validator->fails())
	   	{
	   		$result['success'] = false;
	   		$result['message'] = $validator->messages()->first();
	   	} 
	   	else 
	   	{
	   		$data = array(
	   			'name' => /*$request->name*/ 'test',
	   			'email' => $request->email,
	   			'mes' => $request->message
	   		);

	   		//Reset Email Account for contact us
	   		Config::set('mail.username','website@oneimus.com');
	   		Config::set('mail.password','4W0HnSvRTklb');
	   		
	   		try{
	   			Mail::send('pages.mail_contactus', $data, function($message) {
	   				$message->from('website@oneimus.com');
	   				$message->to('support@oneimus.com', 'Admin')->subject('Contact Us');
	   			});

	   			$result['success'] = true;
	   			$result['message'] = "Message Sent!";
	   		}
	   		catch(\Exception $e){
	   			$result['success'] = false;
	   			$result['message'] = "Message not sent! Sorry for Inconvenience";
	   		}
	   	}

		return response()->json($result);
	}

	public function verification_email($name, $code, $email) {

		$data = array(
			'name' => $name,
			'code' => $code
		);

		//Reset Email Account for verification
		Config::set('mail.username','noreply@oneimus.com');
		Config::set('mail.password','DI4oNhgOIViL');

		try{
			Mail::send('pages.mail_verification', $data, function($message) use($email) {
				$message->from('noreply@oneimus.com');
				$message->to($email, 'User')->subject('OneImus Email Verification');
			});

			$result['success'] = true;
			$result['message'] = "Verification Sent! Please check your email.";
		}
		catch(\Exception $e){
			$result['success'] = false;
			$result['message'] = "Verification not sent! Please contact the support.";
		}

		return $result;
	}

	public function change_email($name, $code, $email) {

		$data = array(
			'name' => $name,
			'code' => $code,
			'email' => $email
		);

		//Reset Email Account for verification
		Config::set('mail.username','noreply@oneimus.com');
		Config::set('mail.password','DI4oNhgOIViL');

		try{
			Mail::send('pages.mail_change', $data, function($message) use($email) {
				$message->from('noreply@oneimus.com');
				$message->to($email, 'User')->subject('OneImus Email Change Request');
			});

			$result['success'] = true;
			$result['message'] = "Request Sent! Please check your email.";
		}
		catch(\Exception $e){
			$result['success'] = false;
			$result['message'] = "Request not sent! Please contact the support.";
		}

		return $result;
	}
}
