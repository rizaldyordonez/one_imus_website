<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Validator;
use Hash;
use PDF;

use App\Repositories\UserRepository;
use App\Models\UserContentLogo;
use App\Models\UserRequirements;
use App\Http\Controllers\MailController;

class ProfileController extends Controller
{

	private $user;
    private $mail;

	function __construct(
        UserRepository $user, 
        MailController $mail
    )
	{
		$this->user = $user;
        $this->mail = $mail;
	}

    public function showProfile(Request $request){

    	$user = Auth::user();

    	$image_name = UserContentLogo::select("logo_image")->first();
        $image_name = $image_name->logo_image;
        $logo = "/images/logo/".$image_name;
        $data['title'] = "ONEIMUS";
        $data['logo'] = $logo;
    	$data['username'] = $user->name;

    	//Get User Log
    	$data['logs'] = $this->user->getLogs($user->id);
    	$data['resume_logs'] = $this->user->getResumeLogs($user->id);
        $data['cv'] = $this->user->getResume($user->id);
        $data['application'] = $this->user->checkResumeById($user->id);
        $data['verified'] = $user->verify;
        
    	return view('pages.profile.user_profile', $data);
    }


    public function showInsertResume(Request $request){

        $user = Auth::user();

        $image_name = UserContentLogo::select("logo_image")->first();
        $image_name = $image_name->logo_image;
        $logo = "/images/logo/".$image_name;
        $data['title'] = "ONEIMUS";
        $data['logo'] = $logo;
        $data['username'] = $user->name;

        //Get User Log
        $data['logs'] = $this->user->getLogs($user->id);
        $data['cv'] = $this->user->getResume($user->id);
        $data['jobs'] = $this->user->getJobLists();
        return view('pages.profile.add_resume.user_add_resume', $data);
    }

    public function showInsertPDF(Request $request){

        $user = Auth::user();

        $image_name = UserContentLogo::select("logo_image")->first();
        $image_name = $image_name->logo_image;
        $logo = "/images/logo/".$image_name;
        $data['title'] = "ONEIMUS";
        $data['logo'] = $logo;
        $data['username'] = $user->name;

        $data['pdf'] = $this->user->checkPDFByUserId($user->id);
        return view('pages.profile.add_resume.user_add_pdf', $data);
    }

    public function showAddWork(Request $request){
        
        $user = Auth::user();

        $image_name = UserContentLogo::select("logo_image")->first();
        $image_name = $image_name->logo_image;
        $logo = "/images/logo/".$image_name;
        $data['title'] = "ONEIMUS";
        $data['logo'] = $logo;
        $data['username'] = $user->name;

        //Get User Log
        $data['logs'] = $this->user->getLogs($user->id);
        
        return view('pages.profile.user_add_work', $data);
    }

    public function showPDF(Request $request){
        
        $user = Auth::user();

        $data['resume'] = $this->user->getResume($user->id);
        $pdfOptions = PDF::setOptions(["dpi" => 150, "defaultFont" => "san-serif"]);
        $pdf = PDF::loadView('pages.profile.pdf.user_view_resume', $data);

        //dd($data);
        return $pdf->stream($user->name.'_resume.pdf');
    }

    public function testPDF(Request $request){
        $user = Auth::user();
        $data['resume'] = $this->user->getResume($user->id);

        return view('pages.profile.pdf.user_view_resume', $data);
    }

    public function showUploadRequirements(Request $request){

        $user = Auth::user();
        $image_name = UserContentLogo::select("logo_image")->first();
        $image_name = $image_name->logo_image;
        $logo = "/images/logo/".$image_name;
        $data['title'] = "ONEIMUS";
        $data['logo'] = $logo;
        $data['username'] = $user->name;
       
        return view('pages.profile.user_upload_requirements', $data);
    }

    public function doSubmitResume(Request $request){
       
        $user = Auth::user();
        $userid = $request->userid;

        $get_resume = $this->user->getResume($userid);
        $resume_id =  $get_resume->resume_id;
        $job_id = $request->job_id;

        $submit_resume = $this->user->submitResumeById($userid,$resume_id,$job_id);
       
        return response()->json($submit_resume);
    }
    
    public function doInsertResume(Request $request){

        $user = Auth::user();
        $user_id = $user->id;
        $data['title'] = "ONEIMUS";
        $data['username'] = $user->name;
       
        $validator = Validator::make($request->all(), [
                "firstname"  => "required|max:20|regex:/^[\pL\s\-]+$/u",
                "middlename" => "required|max:20|regex:/^[\pL\s\-]+$/u",
                "lastname"   => array("required","max:20","regex:/^[A-Za-zñÑ ,.'-]+$/i"), //seems working fine
                // /^[a-zA-Z ]*\.[a-zA-Z ]*$/ -- mandatory .
                // ^([a-z]+[,.]?[ ]?|[a-z]+['-]?)+$ -- unable to use
                // /^[a-z ,.'-]+$/i -- jr. is ok but jr.. is ok too
                // ^(\s)*[A-Za-z]+((\s)?((\'|\-|\.)?([A-Za-z])*))*(\s)*$ - unable to test bcoz laravel
                "resume_age" => "required|max:50|numeric",
                "birthdate"  => "required",
                "nationality" => "required|min:3",
                "civil_status" => "required",
                "gender" => "required|alpha",
                "email"  => "required|email",
                "homeaddress" => "required|max:100",
                "city" => "required",
                "mobilenumber" => "required|min:11|numeric",
                "resume_father" => "nullable",
                "resume_mother" => "nullable",
                "resume_father_contact" => "nullable",
                "resume_mother_contact" => "nullable",
                "region" => "required",
                "work_objective" => "required|max:300|regex:/^[a-zA-ZñÑ\s]*$/",
                "prevDetails.*" => "nullable",
                "choose_available_jobs" => "required",
                "educationalAttainment" => "required",
                "schoolname" => 'required|min:4',
                "fieldOfStudy" => 'nullable',
                "graduateDateFrom" => 'required',
                "graduateDateTo" => 'required',
                "certification" => 'nullable',
                "cert_year_taken" => 'nullable',
                "achievement.*" => 'nullable',
                "skills.name.*" => 'required',
                "skills.year.*" => 'required',
                "ref_data.ref_name.*" => 'required',
                "ref_data.ref_relation.*" => 'required',
                "ref_data.ref_company.*" => 'required',
                "ref_data.ref_position.*" => 'required',
                "ref_data.ref_contact.*" => 'required',
                "ref_data.ref_email.*" => 'required',
                "resume_photo" => 'required|max:5000|Mimes:jpeg,jpg,png|dimensions:width<=2000,height<=1500',
                "cert.*" => 'max:5000|Mimes:jpeg,jpg,png|dimensions:width<=2000,height<=1500'
             ]);

        $msg_image = [
           
            "resume_photo" => "The resume photo was too large. Unable to upload."
        ];

        if ($validator->fails()) {

            $errors = $validator->errors()->toArray();
            // dd(array_key_exists('resume_photo', $errors));
            if (array_key_exists('resume_photo', $errors) && $errors['resume_photo'][0] !== 'The resume photo field is required.') 
            {
                $errors = array_replace_recursive($errors, $msg_image);
            }

            return back()
            ->withErrors($errors)
            ->withInput(); 
        }else{

            echo "success";
            $log1 = "ADD";
            $log2 = "Added New";

            if ($request->id) {
                $log1 = "UPDATED";
                $log2 = 'Update';
            }

            $model = $this->user->insertNewResume($request, $user_id);

            $logs  = $this->user->insertNewLog($user_id, $log1.' CV/Resume', $log2.' CV/Resume');
        }
        
        return redirect()->route("showProfile");
        
        // return view('pages.profile.user_add_resume', $data);
    }

    public function doUploadRequirements(Request $request){
       //dd($request->all());
        $user = Auth::user();

        $validator = Validator::make($request->all(), [
                'requirements'  => 'required',
            ]);

        if ($validator->fails()) {

           $errors = $validator->errors();
           return back()
           ->withErrors($errors)
           ->withInput();
            
        }else{
            if($request->hasFile('requirements')) {
            // dd($request->requirements);
                for($i=0; $i<count($request->requirements); $i++) {

                    $path = file_get_contents($request->requirements[$i]->path());
                    $username = str_slug($request['username'],"-");
                    $destinationPath_website = "../public/images/requirements/".$username;
                    // $destinationPath_website = "../public_html/images/requirements/".$username;

                    if(!is_dir($destinationPath_website)){
                        $dir = mkdir($destinationPath_website, 0777, true);
                    }

                    $image = file_put_contents($destinationPath_website.'/'.$username.'-requirement-'.$i.'.jpg', $path);

                    $destinationPath_admin = "../../one_imus/public/images/requirements/".$username;
                    // $destinationPath_admin = "../../one_imus/public_html/images/requirements/".$username;
                    if(!is_dir($destinationPath_admin)){
                        $dir = mkdir($destinationPath_admin, 0777, true);
                    }

                    $image = file_put_contents($destinationPath_admin.'/'.$username.'-requirement-'.$i.'.jpg', $path);
                }
            }
            $data['user_id'] = $user->id;
            $data['req_name'] = $username;
            $data['req_num'] = count($request->requirements);

            $model = UserRequirements::where('user_id',$user->id)->first();
            
            if (!$model) {
                $model = new UserRequirements;
                $model->user_id = $data['user_id'];
                $model->req_name = $data['req_name'];
                $model->created_at = $this->getDateFormat();
            }
            $model->req_num  = $data['req_num'];
            $model->updated_at = $this->getDateFormat();
            $model->save();

            return redirect()->route('showProfile')->with('success', "Requirements were succesfully uploaded");
        }   
    }

    private function getDateFormat()
    {
        return date("Y-m-d H:i:s");
    }

    public function changePassword(Request $request)
    {
        $user = Auth::user();

        $validator = Validator::make($request->all(), [
                'old_password'  => 'required|min:6',
                'password'      => 'required|confirmed|min:6',
            ]);
        if ($validator->fails()) {

            $result['success'] = false;
            $result['message'] = $validator->messages()->first();
            
        }else{

            //Check if passwor is match
            if (!Hash::check($request->old_password, $user->password)) 
            {
                $result['success'] = false;
                $result['message'] = 'Current password is invalid.';
            }
            else
            {
                //Update password
                $user->password = bcrypt($request->password);
                if (!$user->save())
                {
                    $result['success'] = false;
                    $result['message'] = 'Error changing password. Please try again later.';
                }
                else
                {
                    //Insert Change pass log
                    $log = $this->user->insertNewLog($user->id, 'Change Password', 'Updated new password');

                    $result['success'] = true;
                    $result['message'] = 'Password updated.';
                }
            }
        }
        
        return response()->json($result);
    }

    public function showForgotPassword(Request $request){

       $image_name = UserContentLogo::select("logo_image")->first();
        $image_name = $image_name->logo_image;
        $logo = "/images/logo/".$image_name;
        $data['title'] = "ONEIMUS";
        $data['logo'] = $logo;
        $data['username'] = $user->name;

        return view('pages.auth.forgot-password', $data);
    }

    public function resendEmailVerification(Request $request)
    {
        $data['title'] = "ONEIMUS";

        $image_name = UserContentLogo::select("logo_image")->first();
        $image_name = $image_name->logo_image;
        $logo = "/images/logo/".$image_name;
        $data['logo'] = $logo;

        if (!Auth::check()) 
        {
            $data['status'] = false;
            $data['message'] = "Failed! You must login first!";
        }
        else
        {

            $user = Auth::user();
            //generate verification code
            $veri_code = md5(microtime());

            //isert email verification code
            $verification = $this->user->insertEmailVerificationCode($user->id, $veri_code);
            $email = $this->mail->verification_email($user->name, $veri_code, $user->email);

            $email = $this->user->insertNewLog($user->id, 'Verify Email', 'Verify Email');

            $data['success'] =  $email['success'];
            $data['message'] =  $email['message'];
        }

        return view('pages.profile.email-verify', $data);
    }

    public function changeEmailVerification(Request $request)
    {
        $data['title'] = "ONEIMUS";

        $image_name = UserContentLogo::select("logo_image")->first();
        $image_name = $image_name->logo_image;
        $logo = "/images/logo/".$image_name;
        $data['logo'] = $logo;

        if (!Auth::check()) 
        {
            $data['status'] = false;
            $data['message'] = "Failed! You must login first!";
        }
        else
        {
            $user = Auth::user();
            //generate verification code
            $veri_code = md5(microtime());

            //isert email verification code
            $verification = $this->user->insertEmailVerificationCode($user->id, $veri_code);
            $email = $this->mail->change_email($user->name, $veri_code, $request->email);

            $email = $this->user->insertNewLog($user->id, 'Change Email', 'Change Email');

            $data['success'] =  $email['success'];
            $data['message'] =  $email['message'];
        }

        return response()->json(['success' => $data['success'], 'message' => $data['message']]);
    }

    public function verifyEmail(Request $request){

        $data['title'] = "ONEIMUS";

        $image_name = UserContentLogo::select("logo_image")->first();
        $image_name = $image_name->logo_image;
        $logo = "/images/logo/".$image_name;
        $data['logo'] = $logo;

        if (!isset($request->code)) {
            return redirect('/your-profile');
        }

        //check user if logged in
        if (!Auth::check()) 
        {
            $data['success'] = false;
            $data['message'] = "Failed! You must login first!";
        }
        else
        {
            $user = Auth::user();
            //get code
            $get_code = $this->user->getEmailVerificationCode($user->id, $request->code);

            if ($get_code)
            {
                $verify_code = $this->user->updateEmailVerificationCodeById($user->id, $request->code);
                $verify_user = $this->user->verifyUserById($user->id);

                if ($request->email)
                {
                    $email = $this->user->updateEmailByUserId($user->id, $request->email);
                    
                    if ($email === true) 
                    {
                        $data['success'] = true;
                        $data['message'] = "Success! Email Change and Verified!";
                    }
                    else
                    {
                        $data['success'] = false;
                        $data['message'] = "Something went wrong on updating the email. Please repeat process!";
                    }
                }
                else
                {
                    $data['success'] = true;
                    $data['message'] = "Success! Email Verified!";
                }
            }
            else
            {
                $data['success'] = false;
                $data['message'] = "Failed! Code is not valid anymore. Please resend email verification.";
            }
        }

        return view('pages.profile.email-verify', $data);
    }
}
