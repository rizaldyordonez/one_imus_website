<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use App\Models\UserContentLogo;
use App\Models\UserContentSliders;
use App\Models\UserContentMissionVision;
use App\Models\UserContentJob;
use App\Models\UserContentJobCategory;
use App\Models\UserContentServices;
use App\Models\UserContentCompanyProfile;
use App\Models\Application;
use Illuminate\Support\Facades\Input;
use DB;
use Auth;

class HomeController extends Controller
{

  function __construct(UserRepository $user)
  {
    $this->user = $user;
  }

   //Detecting directory path when upload to hosting service
   public function getPath(){
     echo getcwd();
   }

    public function showIndex(){

        $image_name = UserContentLogo::select("logo_image")->first();
        $slider = UserContentSliders::select("slider_id","slider_img","slider_desc")
                                            ->where("slider_status",1)
                                            ->orderBy("slider_position","asc")
                                            ->get();
        $mission_vision = UserContentMissionVision::select("name","description")
                                                   ->get();
        $available_jobs = UserContentJobCategory::select("name as job_category")
                              ->where("status",1)
                              ->get();
        $services = UserContentServices::select("service_name")
                              ->orderBy("service_position","asc")
                              ->get();
        $company_profile = UserContentCompanyProfile::first();
        
        $image_name = $image_name->logo_image;
        
        $logo = "/images/logo/".$image_name;
        
        $data['title'] = "ONEIMUS";
        $data['logo'] = $logo;
        $data['slider'] = $slider;
        $data['mission_vision'] = $mission_vision;
        $data['available_jobs'] = $available_jobs;
        
        $data['company_profile'] = $company_profile;
        $data['services'] = $services;

    	return view('pages.home', $data);
    }


    public function showLogin(){

      $image_name = UserContentLogo::select("logo_image")->first();
      $image_name = $image_name->logo_image;
      $logo = "/images/logo/".$image_name;
      $data['title'] = "ONEIMUS";
      $data['logo'] = $logo;
    
    	return view('pages.auth.login_modal', $data);
    }

    public function showAbout(){

     $image_name = UserContentLogo::select("logo_image")->first();
     $image_name = $image_name->logo_image;
     $logo = "/images/logo/".$image_name;
     $data['title'] = "ONEIMUS";
     $data['logo'] = $logo;

     return view('pages.about_us',$data);
    }

    public function showAllJobs(Request $request){
      
      $image_name = UserContentLogo::select("logo_image")->first();
      $image_name = $image_name->logo_image;

      $logo = "/images/logo/".$image_name;

      $data['title'] = "ONEIMUS";
      $data['logo'] = $logo;

      if ($request->bool) {
        $available_jobs = UserContentJobCategory::select("name as job_category")
                              ->where("status",1)
                              ->get();
      }
      $data['available_jobs'] = $available_jobs;

      return view('pages.all_available_jobs', $data);
    }

    public function showJobs(Request $request){

      $user = Auth::user();
      //print_r($request->all());
      $image_name = UserContentLogo::select("logo_image")->first();
      $image_name = $image_name->logo_image;
      $logo = "/images/logo/".$image_name;

      $job_category = $request->job_category;
      $job_category_list = (isset($request->job_category_list))? $request->job_category_list : null;
      

      if (isset($job_category) && !isset($job_category_list)) {

        $available_jobs = UserContentJob::leftJoin("partners","partners.id", "=", "job.partner_id")
                          ->leftJoin("job_category","job_category.id", "=", "job.category_id")
                          ->select("job.id","job.partner_id","job.name","job.description","job.jobs_number","partners.name as partners_name","job_category.name as job_category")
                          ->where("job_category.name",$job_category)
                          ->get();

        $data['info'] = false;
        $data['hasActiveResume'] = false;
      }elseif(isset($job_category) && isset($job_category_list)){
        
        $available_jobs = UserContentJob::leftJoin("partners","partners.id", "=", "job.partner_id")
                          ->leftJoin("job_category","job_category.id", "=", "job.category_id")
                          ->select("job.name","job.description","job.id","job.partner_id","job.jobs_number","partners.name as partners_name","job_category.name as job_category")
                          ->where("job_category.name",$job_category)
                          ->where("job.name",$job_category_list)
                          ->get();

        $job_id = UserContentJob::leftJoin("partners","partners.id", "=", "job.partner_id")
                          ->leftJoin("job_category","job_category.id", "=", "job.category_id")
                          ->select("job.id as job_id")
                          ->where("job_category.name",$job_category)
                          ->where("job.name",$job_category_list)
                          ->first();
                          
        if (Auth::check()) {

         $hasActiveResume = $this->user->checkBeforeSubmit($user->id,$request->job_id,$request->partner_id);
         //dd($hasActiveResume);
           if (isset($hasActiveResume['work']) && !is_null($hasActiveResume['work'])) {
              $data['hasActiveResume'] = true;
              $data['resume_status'] = 'You have been fired to this company already!';

           }elseif(isset($hasActiveResume['failed_application']) && !is_null($hasActiveResume['failed_application'])){
              $data['hasActiveResume'] = true;
              $data['resume_status'] = 'You have been rejected to this job already!';

           }elseif(isset($hasActiveResume['existing_job_application']) && !is_null($hasActiveResume['existing_job_application'])){
              $data['hasActiveResume'] = true;
              $data['resume_status'] = 'You have an existing job application!';

           }else{
              $data['hasActiveResume'] = false;
              $data['resume_status'] = 'Submit CV';

           }
        }
        
        $data['info'] = true;
        $data['job_id'] = $job_id->job_id;
      }

      if ($user) {
        $data['id'] = $user->id;
        $data['username'] = $user->name;
        $data['verified'] = $user->verify;
      }else{
        $data['id'] = '';
        $data['username'] = '';
      }

      $data['title'] = "ONEIMUS";
      $data['logo'] = $logo;
      $data['available_jobs'] = $available_jobs;
      $data['job_category'] = $job_category;
      $data['job_category_list'] = $job_category_list;
      

      return view('pages.jobs.jobs-list', $data);
    }
}
